public class AccountPrivacy
{
   public void UpdatePrivacyFlags( Map<Id,Account> AccountsToUpdate,Map<Id,Account> AccountsbeforeUpdate)
  {


for(Account accountToUpdate : AccountsToUpdate.Values())
{
  Account accountBeforeUpdate = AccountsbeforeUpdate.get(accountToUpdate.id);
  
   if(accountBeforeUpdate.Email_Privacy__pc=='Opt out'&&accountToUpdate.Email_Privacy__pc=='Unspecified')
       {
     
         accountToUpdate.Email_Privacy__pc='Opt Out';
         
       }
    if(accountBeforeUpdate.Phone_Privacy__pc=='Opt out'&&accountToUpdate.Phone_Privacy__pc=='Unspecified')
       {
     
         accountToUpdate.Phone_Privacy__pc='Opt Out';
         
       }
     
    if(accountBeforeUpdate.SMS_Privacy__pc=='Opt out'&&accountToUpdate.SMS_Privacy__pc=='Unspecified')
       {
     
         accountToUpdate.SMS_Privacy__pc='Opt Out';
         
       }
     
    if(accountBeforeUpdate.Direct_Mail_Privacy__pc=='Opt out'&&accountToUpdate.Direct_Mail_Privacy__pc=='Unspecified')
       {
     
         accountToUpdate.Direct_Mail_Privacy__pc='Opt Out';
         
       }
           
  
  if(accountBeforeUpdate.personEmail==accountToUpdate.personEmail)
  {
  
       if(accountBeforeUpdate.Email_Privacy__pc=='LFS'&&accountToUpdate.Email_Privacy__pc=='Unspecified')
       {
     
         accountToUpdate.Email_Privacy__pc='LFS';
         
       }
      
       
  }
 
 if(accountBeforeUpdate.PersonHomePhone==accountToUpdate.PersonHomePhone)
  {
  
       if(accountBeforeUpdate.Phone_Privacy__pc=='LFS'&&accountToUpdate.Phone_Privacy__pc=='Unspecified')
       {
     
         accountToUpdate.Phone_Privacy__pc='LFS';
         
       }
       
       
  }
  if(accountBeforeUpdate.PersonMobilePhone==accountToUpdate.PersonMobilePhone)
  {
  
       if(accountBeforeUpdate.SMS_Privacy__pc=='LFS'&&accountToUpdate.SMS_Privacy__pc=='Unspecified')
       {
     
         accountToUpdate.SMS_Privacy__pc='LFS';
         
       }
             
       
  }
  if((accountBeforeUpdate.ShippingStreet==accountToUpdate.ShippingStreet)&&(accountBeforeUpdate.ShippingCity==accountToUpdate.ShippingCity)&&(accountBeforeUpdate.ShippingState==accountToUpdate.ShippingState)&&(accountBeforeUpdate.ShippingPostalCode==accountToUpdate.ShippingPostalCode)&&(accountBeforeUpdate.ShippingCountry==accountToUpdate.ShippingCountry))
  {
  
       if(accountBeforeUpdate.Direct_Mail_Privacy__pc=='LFS'&&accountToUpdate.Direct_Mail_Privacy__pc=='Unspecified')
       {
     
         accountToUpdate.Direct_Mail_Privacy__pc='LFS';
         
       }
       
  }
}

  }
}