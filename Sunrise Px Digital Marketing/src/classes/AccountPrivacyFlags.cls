public class AccountPrivacyFlags {
    
    Public void EmailNSmsOptInOptOutFlags(List<Account> accountList){
        List<Account> EmailOptInAccounts = new List<Account>();
      List<Id> OptInIds = new List<Id>();
      List<Account> EmailOptOutAccounts = new List<Account>();
      List<Id> OptOutIds = new List<Id>();
      Map<Id,Account> mapVoucherAccounts = new Map<Id,Account>();
      Map<Id,Account> mapSMSOptInAccounts = new Map<Id,Account>();
      Map<Id,Account> mapSMSOptOutAccounts = new Map<Id,Account>();
      Map<Id,Account> mapEmailOptInAccounts = new Map<Id,Account>();
      Map<Id,Account> mapEmailOptOutAccounts = new Map<Id,Account>();
      for(Account acc : accountList)
      {
          if(acc.Voucher__c=='Completed')
          {
              mapVoucherAccounts.put(acc.PersonContactId, acc);
          }
        if(acc.Email_Privacy__pc=='LFS')
        {
            // EmailOptInAccounts.add(acc);
            // OptOutIds.add(acc.PersonContactId);
            mapEmailOptInAccounts.put(acc.PersonContactId, acc);
        }
        else if(acc.Email_Privacy__pc=='Opt out')
        {
          //EmailOptOutAccounts.add(acc);
          //OptInIds.add(acc.PersonContactId);
           mapEmailOptOutAccounts.put(acc.PersonContactId, acc);
        }
         if(acc.SMS_Privacy__pc=='LFS')
         {
             mapSMSOptInAccounts.put(acc.PersonContactId, acc);
         }
         else if(acc.SMS_Privacy__pc=='Opt Out')
         {
              mapSMSOptOutAccounts.put(acc.PersonContactId, acc);
         }
      }
      
      if(mapEmailOptInAccounts.size()>0)
      {
        System.debug('opt in loop'+EmailOptInAccounts);
          AddEmailOptInAccountsToCampaign obj = new AddEmailOptInAccountsToCampaign();
         // obj.AddContacts(EmailOptInAccounts,'Email OptIn',OptOutIds);
           obj.AddContacts(mapEmailOptInAccounts.values(),'Email OptIn',mapEmailOptInAccounts.keySet());
       }
      if(mapEmailOptOutAccounts.size()>0)
      {
              System.debug('opt out loop'+EmailOptOutAccounts);
        AddEmailOptInAccountsToCampaign obj = new AddEmailOptInAccountsToCampaign();
         obj.AddContacts(mapEmailOptOutAccounts.values(),'Email OptOut',mapEmailOptOutAccounts.keySet());
      }
        if(mapVoucherAccounts.size()>0)
        {
            AddEmailOptInAccountsToCampaign obj = new AddEmailOptInAccountsToCampaign();
          obj.AddVoucherContacts(mapVoucherAccounts,'Voucher Campaign');
        }
        
        if(mapSMSOptOutAccounts.size()>0)
        {
            AddEmailOptInAccountsToCampaign obj = new AddEmailOptInAccountsToCampaign();
            obj.AddSMSContacts(mapSMSOptOutAccounts.values(),'SMS OptOut',mapSMSOptOutAccounts.keySet());
        }
        if(mapSMSOptInAccounts.size()>0)
        {
            AddEmailOptInAccountsToCampaign obj = new AddEmailOptInAccountsToCampaign();
            obj.AddSMSContacts(mapSMSOptInAccounts.values(),'SMS OptIn',mapSMSOptInAccounts.keySet());
        }
    }
}