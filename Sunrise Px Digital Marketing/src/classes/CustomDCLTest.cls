@isTest
public class CustomDCLTest
{
    static testMethod void CustomDCLTest()
    {        
           Test.startTest();
          
           
         
           RecordType rt = [select id,Name from RecordType where SobjectType='Account' and Name='Patient - US'];    
          
        Account acc = New Account();
        acc.FirstName='TEST31';
        acc.Lastname='TEST41';
        acc.Personemail='xyz@gmail.com';
        acc.PersonMobilePhone = '+919898989898';
        acc.ShippingStreet='vgs';
        acc.ShippingState='KAR';
        acc.ShippingCity='Bangalore';
        acc.shippingPostalCode ='600011';
        acc.ShippingCountry='USA';
        acc.PersonHomePhone='+96969+666666';
        acc.Phone_Privacy__pc='Unspecified';
        acc.SMS_Privacy__pc='LFS';
        acc.Email_Privacy__pc='LFS';
        acc.Direct_Mail_Privacy__pc='LFS';
        acc.TypeOfFile__c='DCL';
        acc.Age__c=28;
        acc.SerialNumber__c='01tg00000043JjFAAU';
        acc.Test_Frequency__pc=7;
        acc.RecordTypeId = rt.id;
        acc.TreatmentMethod__c='Diet/Exercise, Pills, Insulin shots- 3 or more times per day';
        
        insert acc; 
      
         CustomerTempTable__c  cu = new CustomerTempTable__c();
         cu.FirstName__c ='TEST31';
         cu.LastName__c ='TEST41'; 
        // cu.Birthdate__c =Date.newinstance(1975, 9,28);
         cu.Email__c='xyz@gmail.com'; 
         cu.Mobile__c = '+919898989898';
         cu.HomePhone__c='+96969+666666';
         cu.Country__c = 'USA'; 
         cu.PrimaryCity__c = 'Bangalore'; 
         cu.PrimaryStateProvince__c ='KAR'; 
         cu.PrimaryStreet__c='VGS'; 
         cu.PrimaryZipPostalCode__c ='600011';
         cu.PrimaryStreet__c='vgs';
         cu.TypeOfFile__c='DCL';
         cu.MeterMostOftenUsed__c='1 yr';
         cu.Age__c=28;
         cu.SerialNumber__c='01tg00000043JjFAAU';                 
         Cu.DirectMailPrivacy__c='N';
         cu.PhonePrivacy__c='Unspecified';          
         cu.EmailPrivacy__c='Y';
         cu.SMSPrivacy__c='Y';
         cu.OftenTesting__c='1 times per day';
         cu.Diabetes__c='Y';
         cu.ManageMethods__c='Diet/Exercise, Pills, Insulin shots- 3 or more times per day';
          
      
         Date myDate = System.today();
                 if(cu.MeterMostOftenUsed__c=='Less then 1 yr')
                 {
                     
                     acc.MeterRegisteredDate__c = myDate.addMonths(-11);
                 }  
          insert cu; 
        
        
         CustomerTempTable__c  cu1 = new CustomerTempTable__c();
         cu1.FirstName__c ='TEST3';
         cu1.LastName__c ='TEST4'; 
        // cu1.Birthdate__c =Date.newinstance(1975, 9,28);
         cu1.Email__c='x@gmail.com'; 
         cu1.Mobile__c = '+919898989898';
         cu1.HomePhone__c='+96969+666666';
         cu1.Country__c = 'USA'; 
         cu1.PrimaryCity__c = 'Bangalore'; 
         cu1.PrimaryStateProvince__c ='KAR'; 
         cu1.PrimaryStreet__c='VGS'; 
         cu1.PrimaryZipPostalCode__c ='7000001';
         cu1.PrimaryStreet__c='vgs';
         cu1.TypeOfFile__c='DCL';
         cu1.Age__c=30;
         cu1.MeterMostOftenUsed__c='2 yrs';
         Cu1.DirectMailPrivacy__c='Y';
         cu1.PhonePrivacy__c='Unspecified';
         cu1.EmailPrivacy__c='Y';
         cu1.SMSPrivacy__c='Y';
         cu1.SerialNumber__c='01tg00000043Jm1AAE';
         cu1.Diabetes__c='Y';
         cu1.OftenTesting__c='2 times per day';
         cu1.ManageMethods__c='Diet/Exercise, Pills, Insulin shots- 3 or more times per day';
        
         Date myDate1 = System.today();
                 if(cu1.MeterMostOftenUsed__c=='2 yrs')
                 {
                     
                     acc.MeterRegisteredDate__c = myDate1.addMonths(-24);
                 }  
   
        
          insert cu1; 
    
             ProductsSelfReported__c p = new ProductsSelfReported__c();
               p.Account__c = acc.id;
               p.Product__c = '01tg00000043JjFAAU' ;
               p.RegisteredDate__c =acc.MeterRegisteredDate__c;   
                   insert p;  
           
           
              
             ProductsSelfReported__c p1 = new ProductsSelfReported__c();
               p1.Account__c = acc.id;
               p1.Product__c =  '01tg00000043Jm1AAE';
               p1.RegisteredDate__c =acc.MeterRegisteredDate__c;   
                   insert p1;  
                        
              //Test.stopTest();
        
        
    
    // Public static testMethod void testDCL2(){
    // Test.startTest();
       RecordType rp = [select id,Name from RecordType where SobjectType='Account' and Name='Non Patient - US'];  
          
         Account acc1 = New Account();
        acc1.FirstName='TEST71';
        acc1.Lastname='TEST51';
        acc1.Personemail='abc@gmail.com';
        acc1.PersonMobilePhone = '+919898986666';
        acc1.PersonHomePhone='+96969+666622';
        acc1.ShippingStreet='vgs';
        acc1.ShippingState='KAR';
        acc1.ShippingCity='Bangalore';
        acc1.shippingPostalCode ='600011';
        acc1.ShippingCountry='USA';
        acc1.PersonHomePhone='+96969+666666';
        acc1.Phone_Privacy__pc='Unspecified';
        acc1.SMS_Privacy__pc='LFS';
        acc1.Email_Privacy__pc='LFS';
        acc1.Direct_Mail_Privacy__pc='Unspecified';
        acc1.TypeOfFile__c='DCL';
        acc1.Age__c=28;
        acc1.SerialNumber__c='01tb0000002TngdAAC';
        acc1.Test_Frequency__pc=7;
        acc1.RecordTypeId = rp.id;
        acc1.TreatmentMethod__c='Diet/Exercise, Pills, Insulin shots- 3 or more times per day';
        
        insert acc1; 
      
         CustomerTempTable__c  cu2 = new CustomerTempTable__c();
         cu2.FirstName__c ='TEST71';
         cu2.LastName__c ='TEST51'; 
        // cu.Birthdate__c =Date.newinstance(1975, 9,28);
         cu2.Email__c='abc@gmail.com';
         cu2.Mobile__c =  '+919898986666';
         cu2.HomePhone__c='+96969+666622';
         cu2.Country__c = 'USA'; 
         cu2.PrimaryCity__c = 'Bangalore'; 
         cu2.PrimaryStateProvince__c ='KAR'; 
         cu2.PrimaryStreet__c='VGS'; 
         cu2.PrimaryZipPostalCode__c ='600011';
         cu2.PrimaryStreet__c='vgs';
         cu2.TypeOfFile__c='DCL';
         cu2.MeterMostOftenUsed__c='1 yr';
         cu2.Age__c=28;
         cu2.SerialNumber__c='01tb0000002TngdAAC';                 
         Cu2.DirectMailPrivacy__c='Unspecified';
         cu2.PhonePrivacy__c='Unspecified';          
         cu2.EmailPrivacy__c='Y';
         cu2.SMSPrivacy__c='Y';
         cu2.OftenTesting__c='1 times per day';
         cu2.Diabetes__c='N';
         cu2.ManageMethods__c='Diet/Exercise, Pills, Insulin shots- 3 or more times per day';
          
      
         Date myDate2 = System.today();
                 if(cu2.MeterMostOftenUsed__c=='Less then 1 yr')
                 {
                     
                     acc1.MeterRegisteredDate__c = myDate2.addMonths(-11);
                 }  
          insert cu2; 
        
        
         CustomerTempTable__c  cu4 = new CustomerTempTable__c();
         cu4.FirstName__c ='TEST7';
         cu4.LastName__c ='TEST5'; 
        // cu1.Birthdate__c =Date.newinstance(1975, 9,28);
         cu4.Email__c='ab@gmail.com';
         cu4.Mobile__c =  '+919898986666';
         cu4.HomePhone__c='+96969+666622';
         cu4.Country__c = 'USA'; 
         cu4.PrimaryCity__c = 'Bangalore'; 
         cu4.PrimaryStateProvince__c ='KAR'; 
         cu4.PrimaryStreet__c='VGS'; 
         cu4.PrimaryZipPostalCode__c ='7000001';
         cu4.PrimaryStreet__c='vgs';
         cu4.TypeOfFile__c='DCL';
         cu4.Age__c=30;
         cu4.MeterMostOftenUsed__c='2 yrs';
         Cu4.DirectMailPrivacy__c='Unspecified';
         cu4.PhonePrivacy__c='Unspecified';
         cu4.EmailPrivacy__c='Y';
         cu4.SMSPrivacy__c='Y';
         cu4.SerialNumber__c='01tb0000002TmSEAA0';
         cu4.Diabetes__c='N';
         cu4.OftenTesting__c='2 times per day';
         cu4.ManageMethods__c='Diet/Exercise, Pills, Insulin shots- 3 or more times per day';
        
         Date myDate4 = System.today();
                 if(cu4.MeterMostOftenUsed__c=='2 yrs')
                 {
                     
                     acc1.MeterRegisteredDate__c = myDate4.addMonths(-24);
                 }  
   
        
          insert cu4; 
    
             ProductsSelfReported__c p2 = new ProductsSelfReported__c();
               p2.Account__c = acc1.id;
               p2.Product__c = '01tb0000002TngdAAC' ;
               p2.RegisteredDate__c =acc1.MeterRegisteredDate__c;   
                   insert p2;  
           
           
              
             ProductsSelfReported__c p4 = new ProductsSelfReported__c();
               p4.Account__c = acc1.id;
               p4.Product__c =  '01tb0000002TmSEAA0';
               p4.RegisteredDate__c =acc1.MeterRegisteredDate__c;   
                   insert p4;  
      CustomDCL objBatch = new CustomDCL();
         ID batchprocessid = Database.executeBatch(objBatch);
         
        
           Test.stopTest();
     
     
     }
     
}