@isTest
public class CustomIVRTest {

    static testMethod void CustomIVRTest()
    {        
           Test.startTest();
         
           
          
        Account acc = New Account();
        acc.FirstName='TEST31';
        acc.Lastname='TEST41';
        acc.Personemail='xyz@gmail.com';
        acc.PersonMobilePhone = '+919898989898';
        acc.ShippingStreet='vgs';
        acc.ShippingState='KAR';
        acc.ShippingCity='Bangalore';
        acc.shippingPostalCode ='600011';
        acc.ShippingCountry='USA';
        acc.PersonHomePhone='+96969+666666';
        acc.Phone_Privacy__pc='LFS';
        acc.SMS_Privacy__pc='LFS';
        acc.Email_Privacy__pc='LFS';
        acc.Direct_Mail_Privacy__pc='LFS';
        acc.TypeOfFile__c='IVR';
        acc.Age__c=28;
        acc.SerialNumber__c='01tg00000043JjFAAU';
        acc.Test_Frequency__pc=21;
        acc.TreatmentMethod__c = 'insulin shots;other;pills';
        Insert acc; 
        
        List<CustomerTempTable__c> listCustomerTempRecords = new List<CustomerTempTable__c>();
    //  for(integer i=1;i<=4;i++)
      //  {
         CustomerTempTable__c  cu = new CustomerTempTable__c();
         cu.FirstName__c ='TEST31';
         cu.LastName__c ='TEST41'; 
        // cu.Birthdate__c =Date.newinstance(1975, 9,28);
         cu.Email__c='xyz@gmail.com'; 
         cu.Mobile__c =  '+916666666666'; 
         cu.Country__c = 'USA'; 
         cu.PrimaryCity__c = 'Bangalore'; 
         cu.PrimaryStateProvince__c ='KAR'; 
         cu.PrimaryStreet__c='VGS'; 
         cu.PrimaryZipPostalCode__c ='600011';
         cu.PrimaryStreet__c='vgs';
         cu.TypeOfFile__c='IVR';
         cu.MeterMostOftenUsed__c='1 yr';
         cu.Age__c=28;
         cu.SerialNumber__c='01tg00000043JjFAAU';                 
         Cu.DirectMailPrivacy__c='Unspecified';
         cu.PhonePrivacy__c='Unspecified';          
         cu.EmailPrivacy__c='Y';
         cu.SMSPrivacy__c='Y';
         cu.OftenTesting__c=+'1 times per day';
         cu.ManageMethods__c = '3;2;1';
    //      listCustomerTempRecords.add(cu);
         
      //  }        
          insert cu; 
        
        
         CustomerTempTable__c  cu1 = new CustomerTempTable__c();
         cu1.FirstName__c ='TEST31';
         cu1.LastName__c ='TEST41'; 
        // cu1.Birthdate__c =Date.newinstance(1975, 9,28);
         cu1.Email__c='xy@gmail.com'; 
         cu1.Mobile__c =  '+98406+456664'; 
         cu1.Country__c = 'USA'; 
         cu1.PrimaryCity__c = 'Bangalore'; 
         cu1.PrimaryStateProvince__c ='KAR'; 
         cu1.PrimaryStreet__c='VGS'; 
         cu1.PrimaryZipPostalCode__c ='7000001';
         cu1.PrimaryStreet__c='vgs';
         cu1.TypeOfFile__c='IVR';
         cu1.Age__c=30;
         cu1.MeterMostOftenUsed__c='2 yrs';
         Cu1.DirectMailPrivacy__c='Y';
         cu1.PhonePrivacy__c='Y';
         cu1.EmailPrivacy__c='Y';
         cu1.SMSPrivacy__c='Y';
         cu1.SerialNumber__c='01tg00000043Jm1AAE';
         cu1.OftenTesting__c='2 times per day';
         cu1.ManageMethods__c = '3;2;1;';
         insert cu1; 
          
          ProductsSelfReported__c p = new ProductsSelfReported__c();
          p.Account__c = acc.id;
          p.Product__c = '01tg00000043JjFAAU' ;
          p.RegisteredDate__c =acc.MeterRegisteredDate__c;   
          insert p;  
  
          ProductsSelfReported__c p1 = new ProductsSelfReported__c();
          p1.Account__c = acc.id;
          p1.Product__c =  '01tg00000043Jm1AAE';
          p1.RegisteredDate__c =acc.MeterRegisteredDate__c;   
          insert p1;       
   
           

         system.assert(cu.Email__c == acc.Personemail, 'EMail Does not Matches');
         system.assertEquals(acc.Personemail,cu.Email__c,'Email Does Not Matches');
        

         CustomIVR objBatch = new CustomIVR();
         ID batchprocessid = Database.executeBatch(objBatch);
         
        
        Test.stopTest();
        
    }
}