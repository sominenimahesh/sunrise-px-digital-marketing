@isTest
public class DeDuplication_Test
{
    static testMethod void DeDuplicationTest()
    {        
        Test.startTest();
        
        Account accRec = New Account();
        accRec.FirstName ='CUSTFIRSTNAME';
        accRec.Lastname='CUSTLASTNAME';
        accRec.Personemail='test.customer@testcir.com';
        accRec.Age__c=28;
        accRec.PersonMobilePhone = '+98406+456664';
        accRec.shippingStreet ='MBM';
        accRec.ShippingCity='Bangalore';
        accRec.shippingstate='Karnataka';
        accRec.ShippingCountry='India';
        accRec.shippingPostalCode ='600011';
        accRec.PersonHomePhone='+98406+456664';
        accRec.Direct_Mail_Privacy__pc = 'LFS';
        accRec.Phone_Privacy__pc = 'LFS';
        accRec.Email_Privacy__pc = 'LFS';
        accRec.SMS_Privacy__pc = 'LFS';
        accRec.Confirm_Deceased__pc=true;
        Insert accRec; 
           
         CustomerTempTable__c  customeRec = new CustomerTempTable__c();
         
         customeRec.FirstName__c = 'CUSTFIRSTNAME';
         customeRec.LastName__c = 'CUSTLASTNAME'; 
         //customeRec.Age__c = Date.newinstance(1960, 2, 17);
         customeRec.Age__c = 28;
         customeRec.Email__c='test.customer@testcir.com'; 
         customeRec.Mobile__c = '+98406+456664'; 
         customeRec.Country__c = 'India'; 
         customeRec.PrimaryCity__c = 'Bangalore'; 
         customeRec.PrimaryStateProvince__c = 'Karnataka'; 
         customeRec.PrimaryStreet__c='MBM'; 
         customeRec.PrimaryZipPostalCode__c = '600011';
         customeRec.TypeOfFile__c = 'Deceased';
         insert customeRec; 
        
        CustomerTempTable__c  customeRec1 = new CustomerTempTable__c();
         
         customeRec1.FirstName__c = 'CUSTFIRSTNAME';
         customeRec1.LastName__c = 'CUSTLASTNAME'; 
         customeRec1.Age__c = 28;
         customeRec1.Email__c='test.customer@testcir.com'; 
         customeRec1.Mobile__c = '98406456666'; 
         customeRec1.Country__c = 'India'; 
         customeRec1.PrimaryCity__c = 'Bangalore'; 
         customeRec1.PrimaryStateProvince__c = 'Karnataka'; 
         customeRec1.PrimaryStreet__c='MBM'; 
         customeRec1.PrimaryZipPostalCode__c = '600011';
         customeRec1.TypeOfFile__c = 'Deceased';
         insert customeRec1; 

        system.assert(customeRec1.Email__c == accRec.Personemail, 'EMail Does not Matches');
        system.assertEquals(accRec.Personemail,customeRec1.Email__c,'Email Does Not Matches');
              
        DeDuplication deDupObj = new DeDuplication();
        Database.executeBatch(deDupObj);
        
        customtempduplication ctd = new customtempduplication();
        Database.executeBatch(ctd);


           Test.stopTest();
        
    }
}