/** * 
* File Name: GiftBusinessRulesUSService
* Description : This is the service class to provide services for Gift business rules validation & eligibility.
* Copyright : Johnson & Johnson
* * @author : Mahesh Somineni

* Modification Log =============================================================== 
*     Ver  |Date       |Author         |Modification
*    1.0  |23/07/2015  |@msominen@its.jnj.com  |New Class created
* */ 
public class GiftBusinessRulesUSService
{
  public void GiftEligibility(List<Device__c> listDevices)
  {
    System.debug('entered class'+listDevices);
    List<Device__c> DeviceswithSerialNumber = new List<Device__c>();
    List<Id> listAccountIds = new List<Id>();
    list<Id> listNewDeviceIds =  new list<Id>();
    Date warrantyPeriod = Date.newInstance(2015, 12, 01);   
    List<Id> NotEligibleAccountIds = new List<Id>();
    //Product Registration
    
    List<Id> ProductRegisteredIds = new List<Id>();
    
    
    
    for(Device__c d : listDevices)
    {
        System.debug('entered for loop'+listDevices);
      if(d.Serial_Number__c!=null&&d.Registered_Date__c<warrantyPeriod)
      {
        DeviceswithSerialNumber.add(d);
        listAccountIds.add(d.Patient__c);
        listNewDeviceIds.add(d.Id);
          System.debug('Kalaam Devices with serial #'+DeviceswithSerialNumber);
        System.debug('Kalaam eligible ids'+listAccountIds);
      }
       else
       {
           NotEligibleAccountIds.add(d.Patient__c);
           System.debug('Kalaam Not eligible ids'+NotEligibleAccountIds);
       }
       ProductRegisteredIds.add(d.Patient__c);
    }
    List<Account> AddToCampaignAccounts = new List<Account>();
    List<Account> AllRegisteredAccounts = [Select id,FirstName,LastName,GiftEligibility__c,PersonContactId,GiftSourceName__c from Account where id IN:ProductRegisteredIds];
    for(Account ProdAccount:AllRegisteredAccounts )
    {
       if(ProdAccount.GiftSourceName__c == 'WR1113E')
       {
            AddToCampaignAccounts.add(ProdAccount);
       }
    }
    
    if(!AddToCampaignAccounts.isEmpty())
    {
        //String ProductRegistration;
        //List<Campaign> checkProductRegistration = [Select id, name FROM Campaign Where  Name = :ProductRegistration];
        AddEmailOptInAccountsToCampaign classObj = new AddEmailOptInAccountsToCampaign();
        classObj.AddProductRegistration(AddToCampaignAccounts,'Product Registration');
        
    }
    
    
    List<Account> listAccounts = [Select id,FirstName,LastName,GiftEligibility__c,GiftSourceName__c,Primary_Address__c,Primary_Address__r.Location__r.Street__c,Primary_Address__r.Location__r.City__c,Primary_Address__r.Location__r.State__c,Primary_Address__r.Location__r.PostalCode__c,Primary_Address__r.Location__r.Country__c,Full_Shipping_Primary_Address__c from Account where GiftSourceName__c!=null and Primary_Address__c!=null and Confirm_Deceased__pc=false and id IN:listAccountIds and (recordtype.name='Non Patient - US' or recordtype.name='Patient - US')];
    List<Account> NotEligibleAccounts = [Select id,FirstName,LastName,GiftEligibility__c from Account where id IN:NotEligibleAccountIds and (recordtype.name='Non Patient - US' or recordtype.name='Patient - US') ];
    List<Account> updateAccounts = new List<Account>();
    List<Id> filteredAccountIds = new List<Id>();
    Map<Id,Account> AccountMap = new Map<Id,Account>([Select id,FirstName,LastName,GiftSourceName__c from Account where GiftSourceName__c!=null and Primary_Address__c!=null and Confirm_Deceased__pc=false and id IN:listAccountIds and (recordtype.name='Non Patient - US' or recordtype.name='Patient - US')]);
    
    List<Device__c> OldDevices = [Select Patient__c,id,Name,Serial_Number__c from Device__c where Patient__c IN:listAccountIds and Id Not IN:listNewDeviceIds];   
    Map<Id,List<Device__c>> OldDevicesRegisteredMap = new Map<id,List<Device__c>>();
    

    for(Device__c de : OldDevices) {
    if(OldDevicesRegisteredMap.containsKey(de.Patient__c)) {
        List<Device__c> Devices = OldDevicesRegisteredMap.get(de.Patient__c);
        Devices.add(de);
        OldDevicesRegisteredMap.put(de.Patient__c, Devices);
    } else {
        OldDevicesRegisteredMap.put(de.Patient__c, new List<Device__c> { de });
    }
    
   }
    System.debug('Old Devices'+OldDevicesRegisteredMap);   
   List<PriceBookEntry> pbe = [select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where IsActive = True AND Pricebook2Id ='00Bg00000014hXx'];
   List<Device__c> NewlyRegisteredDevices = [Select Patient__c,id,Name,Serial_Number__c from Device__c where Id IN:listNewDeviceIds];
   Map<Id,Device__c> NewlyRegisteredDevicesMap = new Map<Id,Device__c>();
   for(Device__c d : NewlyRegisteredDevices)
   {
     NewlyRegisteredDevicesMap.put(d.Patient__c,d);
   }
   System.debug('New Registered Map '+NewlyRegisteredDevicesMap);
   List<Order> OrdersToCreate = new List<Order>(); 
   List<OrderItem> OrderItemsToCreate = new List<OrderItem>();
   RecordType orderType = [select id,name from recordtype where sObjectType='Order' and name='Customer Order - US'];
 //  Pricebook2 giftPriceBook = [Select id,name from Pricebook2 where name = 'Gift Book - US'];
   
   List<PriceBookEntry> OrderProductEntry = [select id,pricebook2id,Name,Product2.Legacy_record_id__c,product2id from pricebookentry where pricebook2.name='Gift Book - US'];   
   Map<String,PriceBookEntry> MapOrderProductEntry = new Map<String,PriceBookEntry>();
    for(PriceBookEntry pricebookentry:OrderProductEntry)
    {
        MapOrderProductEntry.put(pricebookentry.Product2.Legacy_record_id__c,pricebookentry);
    }
   
     System.debug('Price Book Entries'+MapOrderProductEntry);   
    for(Account a : listAccounts)
    {
      Device__c newDevice = NewlyRegisteredDevicesMap.get(a.id);
      List<Device__C> oDevices = OldDevicesRegisteredMap.get(a.id);
      Set<String> SerialNumList = new Set<String>();
      System.debug('old Devices are '+oDevices);
      System.debug('new device is '+newDevice);
      if(oDevices!=null)
      {
      for(Device__c d : oDevices)
      {
        SerialNumList.add(d.Serial_Number__c);
      }
      }
      System.debug('old Devices are '+SerialNumList);
      if(!SerialNumList.contains(newDevice.Serial_Number__c))
        { 
          PriceBookEntry ProductEntry = MapOrderProductEntry.get(a.GiftSourceName__c);
           System.debug('Product Entry is'+ProductEntry);
          if(ProductEntry!=null)
          {
            
             Order o = new Order();
          o.Accountid = a.id;
          o.EffectiveDate = System.Today();          
          o.Status = 'Synchronized';
          o.RecordTypeId = orderType.Id;
          o.ShippingStreet = a.Primary_Address__r.Location__r.Street__c;
          o.ShippingCity = a.Primary_Address__r.Location__r.City__c;
          o.ShippingState = a.Primary_Address__r.Location__r.State__c;
          o.ShippingPostalCode = a.Primary_Address__r.Location__r.PostalCode__c;
          o.ShippingCountry = a.Primary_Address__r.Location__r.Country__c;
          o.Pricebook2Id = ProductEntry.pricebook2id;
          o.DummyParent__c = a.LastName;
          OrdersToCreate.add(o);   
          
            
           a.GiftEligibility__c = true;
           
           
          updateAccounts.add(a);
          system.debug('kalaam update eligible accounts'+updateAccounts); 
          }
            else
            {
                 a.GiftEligibility__c = false;
                 updateAccounts.add(a);
            }
                
          
        }
        else
        {
            a.GiftEligibility__c = false;
            updateAccounts.add(a);
            system.debug('kalaam update not eligible accounts'+updateAccounts);
        }
        
    }
    System.debug('Orders to create '+OrdersToCreate);
    insert OrdersToCreate;
      
     System.debug('Orders created '+OrdersToCreate);
    //List<PriceBookEntry> OrderProductEntry = [select id,pricebook2id,Name,product2id from pricebookentry where pricebook2.name='Gift Book - US'];
       for(Order o : OrdersToCreate)
    {
         Account accountOrder = AccountMap.get(o.Accountid);
         OrderItem oi = new orderItem();
          oi.OrderId=o.Id;          
          oi.UnitPrice=0;
          oi.Quantity=1;
          for(PriceBookEntry ope : OrderProductEntry)
          {
            String GiftSource = accountOrder.GiftSourceName__c;
             if(GiftSource.equalsIgnorecase(ope.Product2.Legacy_record_id__c))
             {
               oi.PriceBookEntryId=ope.id;
               OrderItemsToCreate.add(oi);  
             }
          }
          
    }
    try
    {
        if(!OrderItemsToCreate.isEmpty())
        {
        insert OrderItemsToCreate;
        }
     }
     catch(Exception e)
     {
     
     }
     if(!NotEligibleAccounts.isEmpty())
     {
         for(Account a : NotEligibleAccounts)
         {
             a.GiftEligibility__c = false;
             
             updateAccounts.add(a);
         }
     }
     try
     {
         system.debug('kalaam update  accounts'+updateAccounts);
         if(!updateAccounts.isEmpty())
         {
             update updateAccounts;
         }
     }
     catch(Exception e)
     {
         
     }
  }
  
}