public class OfferBusinessRulesAdminErrorEmails {
    
    public Static void sendErrorEmailToAdmin(String toEmailAddress, String subject, String emailBody){
        try{
            Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'developer@acme.com'};
                mail.setToAddresses(toAddresses);
            mail.setReplyTo('developer@acme.com');
            mail.setSenderDisplayName('Apex error message');
            mail.setSubject(subject);
            mail.setPlainTextBody(emailBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(Exception ex){
            
        }
    }
}