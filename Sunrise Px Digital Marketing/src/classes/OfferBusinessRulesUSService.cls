/** * 
* File Name: OfferBusinessRulesUSService
* Description : This is the service class to provide services for offer business rules validation & eligibility.
* Copyright : Johnson & Johnson
* * @author : Manmohan Singh |mmo325@its.jnj.com | manmohan.singh1@cognizant.com

* Modification Log =============================================================== 
* 		Ver  |Date 			|Author 				|Modification
*		1.0  |23/07/2015	|@mmo325@its.jnj.com	|New Class created
* */ 
public class OfferBusinessRulesUSService {
    
    public OfferBusinessRulesUSService(List<Account> customerList){
        try{
            Set<String> listOfCampaignCodes = new Set<String>();
            Set<String> sourceSet = new set<String>();
            Set<String> customerIdsList = new set<String>();
            List<Account> noOfferList = new List<Account>();
            List<ProductsSelfReported__c> upsertSelfReportedProducts = new List<ProductsSelfReported__c>();
            Map<ID, Schema.RecordTypeInfo> accountRecordTypeMap = Schema.SObjectType.Account.getRecordTypeInfosById();
            system.debug('accountRecordTypeMap : '+accountRecordTypeMap);
            for(Account eachAccount : customerList){
                if(
                    (
                        (eachAccount.Voucher__c == 'Initiate') &&
                        (eachAccount.SourceName__c).length()>0 &&
                        (eachAccount.CampaignCode__c).length()>0
                    )||(eachAccount.Voucher__c == 'No Offer')
                    
                ){
                    if(eachAccount.Voucher__c == 'No Offer'){
                        Account eachNoOfferAccount = new Account();
                        eachNoOfferAccount.id = eachAccount.id;
                        eachNoOfferAccount.Voucher__c = 'Rejected';
                        eachNoOfferAccount.VoucherStatusComments__c = 'Rejected because no offer customer';
                        noOfferList.add(eachNoOfferAccount);
                    }else{
                        if(
                            (eachAccount.CampaignCode__c!=null && (eachAccount.CampaignCode__c).length()>0) &&
                            (eachAccount.OfferProductId__c!=null && (eachAccount.OfferProductId__c).length()>0) &&
                            (eachAccount.SourceName__c!=null && (eachAccount.SourceName__c).length()>0) &&
                            (eachAccount.SourceMedium__c!=null && (eachAccount.SourceMedium__c).length()>0) &&
                            (eachAccount.MeterRegisteredDate__c!=null)
                        ){
                            listOfCampaignCodes.add(eachAccount.CampaignCode__c);
                        	sourceSet.add(eachAccount.SourceName__c);
                        	customerIdsList.add(eachAccount.id);
                        }else{
                            Account eachNoOfferAccount = new Account();
                        	eachNoOfferAccount.id = eachAccount.id;
                        	eachNoOfferAccount.Voucher__c = 'Rejected';
                        	eachNoOfferAccount.VoucherStatusComments__c = 'Rejected because mandatory fields are not supplied.';
                        	noOfferList.add(eachNoOfferAccount);
                        }
                         
                    }
                    if(OfferBusinessRulesValidations.createSelfRegisteredProducts(eachAccount)!=null){
                        upsertSelfReportedProducts.add(OfferBusinessRulesValidations.createSelfRegisteredProducts(eachAccount));
                    }
                }
            }
            
            // Step 1 --> Create the self registered products in the system.
            if(upsertSelfReportedProducts.size()>0){
                Schema.SObjectField f = ProductsSelfReported__c.Fields.UniqueId__c;
                Database.UpsertResult [] cr = Database.upsert(upsertSelfReportedProducts, f, false);
            }
            if(noOfferList.size()>0){
                try{
                    update noOfferList;
                }catch(Exception ex){
                    System.debug('Exception in No Offer Update : '+ex.getMessage());
                    System.debug('noOfferList : '+noOfferList);
                    
                }
            }
            system.debug('sourceSet : '+sourceSet);
            if(customerList.size()>0){
            if(listOfCampaignCodes.size()>0){
                
                // Step 2 -->  Query all the campaigns and active business rules
                List<campaign> listOfCampaignIds = OfferBusinessRulesSOQL.getCampaingsWithBusinessRules(listOfCampaignCodes);
                
                // Step 2.1 -->  Query all Devices Registered with customers
                Map<String,List<Device__c>> getDevicesByCustomerMap = new Map<String,List<Device__c>>();
                getDevicesByCustomerMap =  OfferBusinessRulesValidations.getDevicesByCustomer(customerIdsList);
                
                // Step 3 --> Query all the transactions records based on campaign and source.
                Map<String,List<Promotion_Transaction__c>> campaignSourceTransactionIdsMap = new Map<String,List<Promotion_Transaction__c>>();
                Map<String,String> campGroupSourceMap = new Map<String,String>();
                if(sourceSet.size()>0){
                    campaignSourceTransactionIdsMap = getCampaignGroupIdMappings(campGroupSourceMap,sourceSet,listOfCampaignCodes);
                }
                
                system.debug('campaignSourceTransactionIdsMap : '+campaignSourceTransactionIdsMap);
                //if(listOfCampaignIds.size()>0){
                
                //listOfPromotionRulesByCampaign : Map
                //This map will contains all the business rules for each campaign.
                
                Map<String,List<Promotion_Rule__c>> listOfPromotionRulesByCampaign = new Map<String,List<Promotion_Rule__c>>();
                Map<String,campaign> campaignMap = new Map<String,campaign>();
                for(campaign eachCampaign : listOfCampaignIds){
                    campaignMap.put(eachCampaign.Campaign_Code__c, eachCampaign);
                    listOfPromotionRulesByCampaign.put(eachCampaign.Campaign_Code__c, eachCampaign.Promotion_Rules__r);
                }
                
                // Processing all customer requests for offer distribution vs rejection
                // Business rules will be applicable.
                
                List<Promotion_Transaction__c> updateList = new List<Promotion_Transaction__c>();
                List<Account> updateAccountList = new List<Account>();
                
                for(Account eachAccount : customerList){
                    //eachAccount.Voucher__c == 'Initiate'
                    if(!(eachAccount.Voucher__c).equalsIgnoreCase('Initiate'))
                        continue;
                    listOfCampaignCodes.add(eachAccount.CampaignCode__c);
                    sourceSet.add(eachAccount.SourceName__c);
                    //<----------------------Logic Starts --------------------------------->
                    if(listOfPromotionRulesByCampaign!=null && listOfPromotionRulesByCampaign.size()>0 
                       && listOfPromotionRulesByCampaign.containsKey(eachAccount.CampaignCode__c)){
                           system.debug('Processing each customer : '+eachAccount.id);
                           Promotion_Transaction__c transactionToUpdate = ProcessEachCustomerRequestForOffer(
                               getDevicesByCustomerMap,campGroupSourceMap,updateAccountList,eachAccount,
                               listOfPromotionRulesByCampaign.get(eachAccount.CampaignCode__c),
                               campaignSourceTransactionIdsMap,
                               accountRecordTypeMap,campaignMap
                           );
                           system.debug('transactionToUpdate: '+transactionToUpdate);
                           if(transactionToUpdate.Status__c=='In Progress')
                               updateList.add(transactionToUpdate);
                       }else{
                           OfferBusinessRulesValidations.TransactionsAreNotAvailableMessage(updateAccountList,eachAccount,' Invalid Campaign Code found.');
                       }
                    
                }
                system.debug('updateList: '+updateList);
                if(updateList.size()>0)
                    update updateList;
                system.debug('updateAccountList: '+updateAccountList);
                if(updateAccountList.size()>0){
                    List<Account> finalListOfUpdateAccount = new List<Account>();
                    Map<String,String> updateAccountMap = new map<String,String>();
                    for(Account eachAccount : updateAccountList){
                        if(updateAccountMap.containsKey(eachAccount.id)){
                            system.debug('Removed from update list as duplicate record found '+eachAccount);
                        }else{
                            updateAccountMap.put(eachAccount.id,eachAccount.id);
                            finalListOfUpdateAccount.add(eachAccount);
                        }
                    }
                    if(finalListOfUpdateAccount.size()>0)
                    update finalListOfUpdateAccount;
                }
                    
                //<----------------------Logic Ends ---------------------------------->
            }
        }
        }Catch(DmlException dmlException){
            System.debug('dmlException : '+dmlException.getMessage());
        }Catch(Exception otherException){
            System.debug('otherException : '+otherException.getMessage());
        }finally{
            
        }
    }
    
    private Promotion_Transaction__c ProcessEachCustomerRequestForOffer(
        Map<String,List<Device__c>> getDevicesByCustomerMap,Map<String,String> campGroupSourceMap,List<Account> updateAccountList,Account customer, 
        List<Promotion_Rule__c> listOfRules,
        Map<String,List<Promotion_Transaction__c>> campaignSourceTransactionIdsMap,
        Map<ID, Schema.RecordTypeInfo> accountRecordTypeMap,Map<String,campaign> campaignMap){
            Promotion_Transaction__c transactionCoupon = new Promotion_Transaction__c();
            boolean businessRulesActiveFlag = true;
            boolean productMatchFlag = true;    
            for(Promotion_Rule__c eachRule : listOfRules){
                try{
                    if(!eachRule.IsActiveRule__c)continue;
                    
                    // flag is used to ensure that min 1 active business rule should be there
                    businessRulesActiveFlag = false;
                    
                    if(eachRule.ProductType__c =='Self Reported'){
                        
                        // Self Reported Check Point - 1 If Meter Match
                        system.debug('Self Reported');
                        if(OfferBusinessRulesValidations.seftReportedMeter(customer,eachRule.ProductUS__c+''))continue;
                        productMatchFlag = false;
                        
                        // Check Point - 2 Meter Age.
                        if(OfferBusinessRulesValidations.selfReportedMeterAge(updateAccountList,customer,eachRule.RegisteredYear__c))continue;
                    }else{
                        system.debug('Registered Product');
                        if(getDevicesByCustomerMap.containsKey(customer.Id)){
                            List<Device__c> deviceList =  getDevicesByCustomerMap.get(customer.Id);
                            if(OfferBusinessRulesValidations.registeredMeterAndAge(updateAccountList,customer,eachRule.RegisteredYear__c,deviceList))continue;
                            productMatchFlag = false;
                        }else{
                            productMatchFlag = false;
                            OfferBusinessRulesValidations.TransactionsAreNotAvailableMessage(updateAccountList, customer, 'No associated Device record found.');
                        }
                    }
                    // Check Point - 3 customerType
                    if(OfferBusinessRulesValidations.customerType(updateAccountList,customer,eachRule.CustomerType__c,accountRecordTypeMap))continue;
                    
                    
                    // Check Point - 4 Customer Age Less than.
                    system.debug(eachRule.id+'dobBefore updateAccountList :'+updateAccountList);
                    if(OfferBusinessRulesValidations.dobBefore(updateAccountList,customer,eachRule.AgeLessthan__c+''))continue;
                     system.debug('after dobBefore updateAccountList :'+updateAccountList);
                    // Check Point - 5 Customer Age Greater than.
                    if(OfferBusinessRulesValidations.dobAfter(updateAccountList,customer,eachRule.AgeGreaterThan__c+''))continue;
                    
                    
                    // Check Point - 6 state.
                    if(OfferBusinessRulesValidations.stateValidaiton(updateAccountList,customer,eachRule.ShippingState__c+''))continue;
                    
                    // Check Point - 7 country.
                    if(OfferBusinessRulesValidations.countryValidaiton(updateAccountList,customer,eachRule.ShippingCountry__c+''))continue;
                    
                    // Check Point - 8 treatment Type.
                    if(OfferBusinessRulesValidations.treatmentMethodValidaiton(updateAccountList,customer,eachRule.TreatmentMethod__c+''))continue;
                    
                    // Check Point - 9 Test Frequency.
                    if(OfferBusinessRulesValidations.testFrequencyPerWeek(updateAccountList, customer, eachRule.FrequencyPerWeek__c))continue;
                    
                    
                    // Household vs Per Person --> Check Point - 10/11 offer Count + Duration
                    if(eachRule.IsHousehold__c){
                        system.debug('Household');
                        if(OfferBusinessRulesValidations.offerCountByDurationPerHouseHold(updateAccountList,customer,eachRule.Days__c,eachRule.AllowedProductCount__c+''))continue;
                    }else{
                        system.debug('Person');
                        if(OfferBusinessRulesValidations.offerCountByDuration(updateAccountList,customer,eachRule.Days__c,eachRule.AllowedProductCount__c+''))continue;
                    }
                    //<------------------Coupon Update------------------------->
                    if(campGroupSourceMap.containsKey(customer.CampaignCode__c+'#'+customer.SourceName__c)){
                        String campSourceStr = campGroupSourceMap.get(customer.CampaignCode__c+'#'+customer.SourceName__c);
                        List<Promotion_Transaction__c> eachTransactionListForSource = campaignSourceTransactionIdsMap.get(campSourceStr);
                        system.debug('eachTransactionListForSource :'+eachTransactionListForSource);
                        if(eachTransactionListForSource!=null && eachTransactionListForSource.size()>0){
                            transactionCoupon = eachTransactionListForSource.get(0);
                            OfferBusinessRulesValidations.allocateTransaction(updateAccountList,customer, 
                                                                              transactionCoupon,campaignMap);
                            //transactionCoupon.AccountRowId__c = customer.Id;
                            transactionCoupon.Status__c = 'In Progress';
                            eachTransactionListForSource.remove(0);
                            transactionCoupon.ReceivedDate__c = System.today();
                            transactionCoupon.Account__c = customer.Id;
                            transactionCoupon.SourceName__c = customer.SourceName__c;
                            transactionCoupon.Medium__c = customer.SourceMedium__c;
                            campaignSourceTransactionIdsMap.put(customer.CampaignCode__c+'#'+customer.SourceName__c, eachTransactionListForSource);
                            break;
                        }else{
                            OfferBusinessRulesValidations.TransactionsAreNotAvailable(updateAccountList,customer);
                        }
                    }else{
                        OfferBusinessRulesValidations.TransactionsAreNotAvailable(updateAccountList,customer);
                    }
                    
                    //<------------------Coupon Update------------------------->
                    
                    
                }catch(Exception ex){
                    System.debug('Exception in OfferBusinessRulesUSService.ProcessEachCustomerRequestForOffer : '+ex.getMessage());
                    System.debug('customer Info : '+customer);
                    System.debug('Business Rule : '+eachRule);
                }
            }
            if(businessRulesActiveFlag){
                OfferBusinessRulesValidations.TransactionsAreNotAvailableMessage(updateAccountList,customer,'No Business Rule defined.');
            }
            if(productMatchFlag){
                OfferBusinessRulesValidations.TransactionsAreNotAvailableMessage(updateAccountList,customer,'No Business Rule for meter.');
            }
            system.debug('transactionCoupon: '+transactionCoupon);
            return transactionCoupon;
        }
    public static Map<String,List<Promotion_Transaction__c>> getCampaignSourceTransactionIds(String campaignCode, String groupName){
        Map<String,List<Promotion_Transaction__c>> campaignSourceMap = new Map<String,List<Promotion_Transaction__c>>();
        List<Promotion_Transaction__c> transactionList = OfferBusinessRulesSOQL.getAvailableTransactions(campaignCode,groupName);
        for(Promotion_Transaction__c eachTransaction : transactionList){
            if(!campaignSourceMap.containskey(eachTransaction.Campaign__r.Campaign_Code__c+'#'+eachTransaction.Group__c)){
                List<Promotion_Transaction__c> eachNewList = new List<Promotion_Transaction__c>();
                eachNewList.add(eachTransaction);
                campaignSourceMap.put(eachTransaction.Campaign__r.Campaign_Code__c+'#'+eachTransaction.Group__c,eachNewList);
            }else{
                List<Promotion_Transaction__c> eachNewList = campaignSourceMap.get(eachTransaction.Campaign__r.Campaign_Code__c+'#'+eachTransaction.Group__c);
                eachNewList.add(eachTransaction);
                campaignSourceMap.put(eachTransaction.Campaign__r.Campaign_Code__c+'#'+eachTransaction.Group__c,eachNewList);
            }
        }
        return campaignSourceMap;
    }
    public Map<String,List<Promotion_Transaction__c>> getCampaignGroupIdMappings(Map<String,String> campGroupSourceMap,Set<String> sourceList,Set<String> campList){
        Map<String,List<Promotion_Transaction__c>> campaignSourceTransactionIdsMap = new Map<String,List<Promotion_Transaction__c>>();
        List<Promotion_Item__c> proList = OfferBusinessRulesSOQL.getSourceGroupMappings(sourceList,campList);
        //Campaign__r.Campaign_Code__c
        for(Promotion_Item__c eachPromoItems : proList){
            String eachSources = eachPromoItems.Sources__c;
            if(eachSources.length()>0 && eachSources.contains(';')){
                String[] eachSourceArr = eachSources.split(';');
                for(String eachSourceStr: eachSourceArr){
                    if(!campGroupSourceMap.containsKey(eachPromoItems.Campaign__r.Campaign_Code__c+'#'+eachSourceStr)){
                        campGroupSourceMap.put(eachPromoItems.Campaign__r.Campaign_Code__c+'#'+eachSourceStr, eachPromoItems.Campaign__r.Campaign_Code__c+'#'+eachPromoItems.Group__c);
                    }
                }
            }else if (eachSources.length()>0){
                if(!campGroupSourceMap.containsKey(eachPromoItems.Campaign__r.Campaign_Code__c+'#'+eachSources)){
                    campGroupSourceMap.put(eachPromoItems.Campaign__r.Campaign_Code__c+'#'+eachSources, eachPromoItems.Campaign__r.Campaign_Code__c+'#'+eachPromoItems.Group__c);
                }
            }
        }
        if(campGroupSourceMap.size()>0){
            Set<String> sourceSets = new Set<String>();
            List<String> sourcesList = campGroupSourceMap.values();
            for(String eachString : sourcesList){
                sourceSets.add(eachString);
            }
            
            for(String eachCampaignIdsAndSourceIds : sourceSets){
                campaignSourceTransactionIdsMap.putAll(OfferBusinessRulesUSService.getCampaignSourceTransactionIds(eachCampaignIdsAndSourceIds.substringBefore('#'),eachCampaignIdsAndSourceIds.substringAfter('#')));
            }
        }
        return campaignSourceTransactionIdsMap;
    }
}