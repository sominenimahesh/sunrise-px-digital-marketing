public class PopulateOrderStatus
{
    public static void updateOrderStatusValue(List<order> orderValues)
    {
        set<String> orderStatusValues = new set<String>();
        
        for(order orderStatus : orderValues)
        {
            orderStatusValues.add(orderStatus.OrderItemStatus__c);
        }
        
        system.debug('orderStatusValues ==>' + orderStatusValues);
        
        List<order> listOrderStatusValues = [Select id, OrderItemStatus__c FROM order where OrderItemStatus__c IN:orderStatusValues];
        
        system.debug('listOrderStatusValues ==>' + listOrderStatusValues);
        
        Map<String,String> OrderItemStatusValues = new Map<String,String>();
        OrderItemStatusValues.put('B' , 'Backordered');  
        OrderItemStatusValues.put('C' , 'Cancelled'); 
        OrderItemStatusValues.put('P' , 'Pending Shipment (Unshipped)'); 
        OrderItemStatusValues.put('R' , 'Rejected');  
        OrderItemStatusValues.put('S' , 'Shipped');  
        OrderItemStatusValues.put('T' , 'Returned');   
        
        for(order updateOrderStatus : orderValues)
        {
        
            //if(OrderItemStatusValues!=null && OrderItemStatusValues.size()>0 )
            //{
                if(OrderItemStatusValues.containskey(updateOrderStatus.OrderItemStatus__c))
                {
                    updateOrderStatus.OrderItemStatus__c = OrderItemStatusValues.get(updateOrderStatus.OrderItemStatus__c);
                    system.debug('updated values==>' + updateOrderStatus.OrderItemStatus__c);
                }
           // }
        
         }
    }
}