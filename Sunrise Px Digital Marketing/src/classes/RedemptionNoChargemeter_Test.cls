@isTest
public class RedemptionNoChargemeter_Test
{
    static testMethod void RedemptionNoChargemeterTest()
    {        
         Test.startTest();
        
         Campaign campaignRec =  new Campaign();
         //campaignRec.Previous_Campaign__c = '701g0000000MVmF';
         RecordType rt = [select id,Name from RecordType where SobjectType='Campaign' and Name='Promotion' Limit 1];
         campaignRec.RecordTypeId = rt.id;
         campaignRec.name = 'OT.com';
         insert campaignRec;
        
         Product2  proRec = new Product2();
         proRec.name='Accu-Chek';
         insert proRec;
        
   
        
         Promotion_Rule__c rule = new Promotion_Rule__c();
         rule.name = 'Accu-chek';
         rule.Product__c = proRec.id;
         rule.Campaign__c = campaignRec.id;
         rule.Type__c = 'self';
         Insert rule;
           
        
         Promotion_Item__c GroupRec =  new Promotion_Item__c();
         GroupRec.Legacy_Record_Id__c = 'LVAAV378';
         GroupRec.Name = 'search Bucket';
         GroupRec.Promotion_Rule__c =rule.id;
         Insert GroupRec ;
        
         Pharmacy__c pharmacyRec = new Pharmacy__c();
         pharmacyRec.Name = 'DIABETES SPECIALTY CENTER';
         pharmacyRec.NABP__c ='4607529';
         Insert pharmacyRec;
        
        RecordType accRt = [SELECT id , name FROM RecordType where SobjectType='Account' and Name='Business Account - SK' Limit 1];
         Account accRec = new Account();              
         accRec.RecordTypeId = accRt.id;
         accRec.Name = 'ShankarTest';
         //accRec.PersonEmail = 'test@gmail.com';
         accRec.shippingstreet = 'Chruch Street';
         accRec.shippingCity = 'Bangalore';
         accRec.shippingstate = 'Karnataka';
         accRec.shippingPostalCode = '560054';
         accRec.shippingCountry = 'India';
         Insert accRec;
         
         RecordType conrt = [select id,Name from RecordType where SobjectType='contact' and Name='HCP - SK' Limit 1];
         contact contactRec = new contact();
         contactRec.Accountid= accRec.id;
         contactRec.RecordTypeId = conrt.id;
         contactRec.lastName = 'Shivu';
         contactRec.HCP__c = '1871566695';
         contactRec.MailingStreet = 'Mathikere';
         contactRec.MailingCity = 'Banglore';
         contactRec.MailingCountry = 'India';
         Insert contactRec; 
         
              
         Promotion_Transaction__c VoucherRec = New Promotion_Transaction__c();
         VoucherRec.Promotion_Item__c = GroupRec.id;
         VoucherRec.VoucherNumber__c = 'NOCHARGEMETR';
         VoucherRec.DrugName__c = 'ULTRA 2014 ONETOUCH PLATFORM CARD CATALINA'; 
         VoucherRec.GroupNumber__c = 'LVAAV378'; 
         VoucherRec.CopayAmount__c = 0; 
         VoucherRec.PatientAge__c = 25; 
         VoucherRec.Pharmacy__c = pharmacyRec.id;
         VoucherRec.ClaimTypePicklist__c = '+'; 
         VoucherRec.MailOrderIndicatorPickList__c = '1'; 
         VoucherRec.PatientGenderPickllist__c = 'Male'; 
         VoucherRec.OtherCoverageCode__c = '0'; 
         VoucherRec.DEA__c = 'BS6075368'; 
         VoucherRec.VoucherReceivedDate__c = Date.Today(); 
         VoucherRec.RXOrigin__c = '6261538'; 
         VoucherRec.NDC__c = '53885044801'; 
         VoucherRec.TotalAmountPaid__c = 18.30000; 
         VoucherRec.HCP__c = contactRec.id;  
         Insert VoucherRec ; 
         
           
         RedemptionTempTable__c  redempRec= new RedemptionTempTable__c();
         redempRec.CardIDNumber__c = 'NOCHARGEMETR';
         redempRec.DrugName__c = 'ULTRA 2014 ONETOUCH PLATFORM CARD CATALINA'; 
         redempRec.GroupNumber__c = 'LVAAV378'; 
         redempRec.CopayAmount__c = 0; 
         redempRec.PatientAge__c = 25; 
         redempRec.PharmacyNABP__c = '4607529'; 
         redempRec.ClaimType__c = '+'; 
         redempRec.MailOrderIndicator__c = '1'; 
         redempRec.PatientGender__c = 'Male'; 
         redempRec.OtherCoverageCode__c = '0'; 
         redempRec.DEA__c = 'BS6075368'; 
         redempRec.VoucherReceivedDate__c = Date.Today(); 
         redempRec.RXOrigin__c = '6261538'; 
         redempRec.NDC__c = '53885044801'; 
         redempRec.TotalAmountPaid__c = 18.30000; 
         redempRec.HCP__c = '1871566695';
         Insert redempRec; 
         
        
        

        system.assert(redempRec.GroupNumber__c == VoucherRec.GroupNumber__c, 'GroupNumber Does not Matches');
        system.assertEquals(contactRec.id,VoucherRec.HCP__c,'HCP Does Not Matches');
              
        RedemptionNoChargemeter redempNoMeter = new RedemptionNoChargemeter();
        Database.executeBatch(redempNoMeter);
        
        

        Test.stopTest();
        
    }
}