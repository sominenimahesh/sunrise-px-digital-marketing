@isTest
public class TestGiftBusinessRulesUSService
{
 private static testmethod void insertAccount()
 {
 
   Date ExpiryDate = Date.newInstance(2015, 11,01);
   RecordType rt = [select id,Name from RecordType where SobjectType='Account' and Name='Patient - US' Limit 1];
   Account testAccount = new Account();
   testAccount.LastName = 'Mahesh';
   testAccount.GiftSourceName__c = 'lancet';
   testAccount.personEmail = 'naidumahesh5@gmail.com';
   testAccount.shippingstreet = 'Dalal Street';
   testAccount.shippingCity = 'Bangalore';
   testAccount.shippingstate = 'Karnataka';
   testAccount.shippingPostalCode = '560045';
   testAccount.shippingCountry = 'India';
   testAccount.Email_Privacy__pc='Opt out';
   testAccount.SMS_Privacy__pc = 'Opt out';
   testAccount.Voucher__c = 'Completed';
   testAccount.ExpireDate__c = ExpiryDate;
   testAccount.VoucherNumber__c = '1234567';
   testAccount.RecordTypeId = rt.id;
   
   insert testAccount;
     
   Account acc = [select id,LastName,GiftSourceName__c,personEmail,shippingstreet,shippingCity,shippingstate,shippingPostalCode,Email_Privacy__pc,SMS_Privacy__pc from Account where id=:testAccount.id];
   
   acc.Email_Privacy__pc='LFS';
   acc.SMS_Privacy__pc = 'LFS';
   acc.Voucher__c = 'Completed';
   acc.ExpireDate__c = ExpiryDate;
   acc.VoucherNumber__c = '1234568';
   acc.RecordTypeId = rt.id;
   
     update acc;
    
   Account testAccount1 = new Account();
   testAccount1.LastName = 'Mahes';
   testAccount1.GiftSourceName__c = 'lancet';
   testAccount1.personEmail = 'naidumahsh5@gmail.com';
   testAccount1.shippingstreet = 'Dalal Street';
   testAccount1.shippingCity = 'Bangalore';
   testAccount1.shippingstate = 'Karnataka';
   testAccount1.shippingPostalCode = '560045';
   testAccount1.shippingCountry = 'India';
   testAccount1.Email_Privacy__pc='LFS';
   testAccount1.SMS_Privacy__pc = 'LFS';
   testAccount1.Voucher__c = 'Completed';
   testAccount1.ExpireDate__c = ExpiryDate;
   testAccount1.VoucherNumber__c = '1234567';
   testAccount1.RecordTypeId = rt.id;
   
   insert testAccount1;
   
   Product2 testProduct = new Product2();
   testProduct.Name = 'lancet';
   testProduct.CurrencyIsoCode='USD';
   testProduct.isActive=true;  
   testProduct.Serial_Number_Mask__c = 'V???????';
   testProduct.Market__c = 'US';
   
   insert testProduct; 
   
    Id pricebookId = Test.getStandardPricebookId();
    
    PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = testProduct.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Gift Book - US', isActive=true);
        insert customPB;
        
         PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = testProduct.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
   
   Date warrantyPeriod = Date.today();
     
   Device__c testDevice = new Device__c();
   testDevice.Serial_Number__c = 'V1234567';
   testDevice.Product__c = testProduct.Id;
   testDevice.Patient__c = testAccount.Id;
    testDevice.Registered_Date__c = warrantyPeriod;
   
   insert testDevice;
   
   Device__c testDevice1 = new Device__c();
   testDevice1.Serial_Number__c = 'V1234567';
   testDevice1.Product__c = testProduct.Id;
   testDevice1.Patient__c = testAccount.Id;
   testDevice1.Registered_Date__c = warrantyPeriod;
   
     insert testDevice1;
   
 }
    private static testmethod void mergeAccounts()
 {
   Test.startTest();
   RecordType rt = [select id,Name from RecordType where SobjectType='Account' and Name='Patient - US' Limit 1];
   Account testAccount = new Account();
   testAccount.FirstName = 'S';
   testAccount.LastName = 'Mahesh';
   testAccount.GiftSourceName__c = 'lancet';
   testAccount.personEmail = 'naidumahesh5@gmail.com';
   testAccount.shippingstreet = 'Dalal Street';
   testAccount.shippingCity = 'Bangalore';
   testAccount.shippingstate = 'Karnataka';
   testAccount.shippingPostalCode = '560045';
   testAccount.shippingCountry = 'India';
   testAccount.Email_Privacy__pc='LFS';
   testAccount.Phone_Privacy__pc = 'LFS';
   testAccount.RecordTypeId = rt.id;
   testAccount.PersonHomePhone = '+98406+456764';
   testAccount.Voucher__c = 'Completed';   
   testAccount.VoucherGroupId__c='1234';
   testAccount.VoucherNumber__c = '7884512123';
   testAccount.ExpireDate__c = Date.newinstance(2015, 12, 11);
     
   insert testAccount;
   
   Account testAccount1 = new Account();
   testAccount1.FirstName = 'S';
   testAccount1.LastName = 'Mahesh';
   testAccount1.GiftSourceName__c = 'lancet';
   testAccount1.personEmail = 'naidumahsh5@gmail.com';
   testAccount1.shippingstreet = 'Dalal Street';
   testAccount1.shippingCity = 'Bangalore';
   testAccount1.shippingstate = 'Karnataka';
   testAccount1.shippingPostalCode = '560045';
   testAccount1.shippingCountry = 'India';
   testAccount1.Email_Privacy__pc='UnSpecified';
   testAccount1.Phone_Privacy__pc ='UnSpecified';
   testAccount1.RecordTypeId = rt.id;
   testAccount1.PersonHomePhone = '+98406+456664';
   
   insert testAccount1;
 /*  MergeDuplicateContacts controller = new MergeDuplicateContacts();
   //Execute Batch
   Database.executeBatch(controller , 200);*/
   Test.stopTest();
   
 }
}