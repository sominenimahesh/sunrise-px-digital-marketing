public class UpdateDOBBasedOnAge {
    public UpdateDOBBasedOnAge(List<Account> processedAccounts){
        for(Account eachAccount : processedAccounts){
            if(eachAccount.Age__c !=null){
               eachAccount.PersonBirthdate = system.today().addYears(-Integer.valueOf(eachAccount.Age__c));
            }
        }
    }
}