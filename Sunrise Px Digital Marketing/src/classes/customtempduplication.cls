global class customtempduplication implements Database.Batchable<sObject>,Database.Stateful 
{
    global final String query;
    global final String BRC;
    
    List<CustomerTempTable__c> deleteProcessedTempCustomers = new List<CustomerTempTable__c>();
       
    global customtempduplication()
    {  
      // query='Select id,Name,Primary_Address__c,Primary_Address__r.Location__r.Street__c,Primary_Address__r.Location__r.City__c,Primary_Address__r.Location__r.State__c,Primary_Address__r.Location__r.PostalCode__c,Primary_Address__r.Location__r.Country__c,Full_Shipping_Primary_Address__c , Email from Account ';
       
       BRC = 'BRC';
     query='select id , FirstName__c,Title__c,Suffix__c,Birthdate__c,LastName__c,Contactmethod__c,HowManyShots__c,OftenTesting__c,SerialNumber__c,HomePhone__c,ManageMethods__c,Email__c,DirectMailPrivacy__c,EmailPrivacy__c,PhonePrivacy__c,SMSPrivacy__c, Mobile__c, Address__c, Country__c, PrimaryCity__c, PrimaryStateProvince__c, PrimaryStreet__c, PrimaryZipPostalCode__c,Gender__c,TypeOfFile__c  From CustomerTempTable__c where TypeOfFile__c=:BRC';
       
       system.debug('customer==>'+query);
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
      return Database.getQueryLocator(query);
    }

   global void execute(Database.BatchableContext BC, List<Sobject> scope)
   {
       
        List<CustomerTempTable__c> ListofTempCustomer = (List<CustomerTempTable__c>)scope;
        RecordType rt = [select id,Name from RecordType where SobjectType='Account' and Name='Patient - US' Limit 1];  
          String Fname ;
          String UFname ; 
          
          String Lname;
          String ULname;
         
         List<Account> insertAccounts =new List<Account>();       
         List<Account> updateAccounts =new List<Account>();
         List<Device__C> insertDevices =new List<Device__C>();
         List<Device__c> OldDevices = [Select Patient__c,id,Serial_Number__c,Product__C from Device__c];   
         Map<Id,List<Device__c>> OldDevicesRegisteredMap = new Map<id,List<Device__c>>();
            

        for(Device__c de : OldDevices) {
            if(OldDevicesRegisteredMap.containsKey(de.Patient__c)) {
            List<Device__c> Devices = OldDevicesRegisteredMap.get(de.Patient__c);
            Devices.add(de);
            OldDevicesRegisteredMap.put(de.Patient__c, Devices);
             } else {
             OldDevicesRegisteredMap.put(de.Patient__c, new List<Device__c> { de });
            }
        }
       
        for(CustomerTempTable__c eachCustomerTempRec : ListofTempCustomer)
        {
      
             Fname = eachCustomerTempRec.FirstName__c;
             UFname = Fname.toUpperCase();
             
             system.debug('UFname ==>'+UFname );
             
             Lname = eachCustomerTempRec.LastName__c;
             ULname = Lname.toUpperCase();
             
              
              system.debug('address ==>'+eachCustomerTempRec.Address__c);
             
              List<Account> ListAccount =[ Select id , name, PersonEmail, PersonBirthdate,ShippingCountry, 
                                         ShippingState,ShippingCity,ShippingStreet,ShippingPostalCode,TypeOfFile__c,SerialNumber__c
             From Account where FirstName =:UFname AND LastName =:ULname 
              AND PersonEmail=:eachCustomerTempRec.Email__c Limit 1];
               
            
 
              system.debug('ListAccount==>'+ListAccount);
              CustomerTempTable__c c = new CustomerTempTable__c();
            
              if(listAccount.size()> 0)
               {
                   Boolean DeviceExisted = false;
                 System.debug('entered if loop');
                 try{ Account ac = new Account();
                 ac.Id = ListAccount[0].id;
                 ac.Direct_Mail_Privacy__pc =eachCustomerTempRec.DirectMailPrivacy__c;
                 
                 ac.Email_Privacy__pc =eachCustomerTempRec.EmailPrivacy__c;
                 
                 ac.Phone_Privacy__pc =eachCustomerTempRec.PhonePrivacy__c;
                 
                 ac.SMS_Privacy__pc =eachCustomerTempRec.SMSPrivacy__c;
                 
                 ac.PersonEmail=eachCustomerTempRec.Email__c;
                 
                 ac.LastName=eachCustomerTempRec.LastName__c;
                  
                 ac.FirstName=eachCustomerTempRec.FirstName__c;
                
                 ac.ShippingStreet=eachCustomerTempRec.PrimaryStreet__c;
                 
                 ac.ShippingState=eachCustomerTempRec.PrimaryStateProvince__c;
                 
                 ac.ShippingPostalCode=eachCustomerTempRec.PrimaryZipPostalCode__c;
                 
                 ac.ShippingCountry='US';
                 ac.PersonBirthdate = eachCustomerTempRec.Birthdate__c;
                 ac.ShippingCity=eachCustomerTempRec.PrimaryCity__c;
                 
                 ac.SerialNumber__c = eachCustomerTempRec.SerialNumber__c;
                  ac.PersonHomePhone = eachCustomerTempRec.HomePhone__c;
                  ac.PersonMobilePhone = eachCustomerTempRec.Mobile__c;
                  ac.PersonTitle=eachCustomerTempRec.Title__c;
                 ac.Suffix=eachCustomerTempRec.Suffix__c;
                 ac.TypeOfFile__c=eachCustomerTempRec.TypeOfFile__c;
                 
//**************** Manage Methods >> IT will convert value to treatment method as multipicklist *****************************************
          
           String convertManagemethod = eachCustomerTempRec.ManageMethods__c;
            convertManagemethod = convertManagemethod.trim();
                convertManagemethod = convertManagemethod.replaceAll('(\\s+)','');
                  
                   system.debug('convertManagemethod ==> ' +convertManagemethod );
                  
                 String[] convertedMethods = convertManagemethod.split(';');
                 system.debug('convertedMethods ==> ' +convertedMethods );
                  //final string NUMBER_OF_HOURS_IN_A_DAY = 'Insulin';
                  //constant ss = insulin;
                   //final Integer thirdNumber = 3;
                  // final Integer twonumber = 2;
                  // final Integer onenumber = 1;
                  Map<Integer,String> mapManageMethods = new Map<Integer,String>();
                  mapManageMethods.put(1,'Diet');
                  mapManageMethods.put(2,'Pills');
                  mapManageMethods.put(3,'Insulin Shots');
                  mapManageMethods.put(4,'Insulin Pump');
                  mapManageMethods.put(5,'Exercise');
                  mapManageMethods.put(6,'Other');
                  mapManageMethods.put(7,'Shots other than insulin');
                  
                  string treatMethod='';
                  for(String s:convertedMethods)
                  {
                    String s1 = mapManageMethods.get(Integer.valueof(s));
                    system.debug('s1  ==> ' +s1 );
                    
                    //acc.TreatmentMethod__c+=s1+'+;';
                    treatMethod+= s1+';';
                    //treatMethod = s1;
                  }
                 ac.TreatmentMethod__c = treatMethod ;
                 
//********************************** Managemethods ends here ***************************************************************************

         
         if(eachCustomerTempRec.OftenTesting__c=='4+ times per day')
                {
              
                ac.Test_Frequency__pc=28;
                  }                
                  if(eachCustomerTempRec.OftenTesting__c=='3 times per day')
                {
              
                ac.Test_Frequency__pc=21;
                  }
              
                    if(eachCustomerTempRec.OftenTesting__c=='2 times per day')
                {
              
                ac.Test_Frequency__pc=14;
                  }
                 
                     if(eachCustomerTempRec.OftenTesting__c=='1 time per day')
                {
              
                ac.Test_Frequency__pc=7;
                  }
                      if(eachCustomerTempRec.OftenTesting__c=='4-6 times per week')
                {
              
                ac.Test_Frequency__pc=5;
                  }
                        if(eachCustomerTempRec.OftenTesting__c=='1-3 times per week')
                {
              
                ac.Test_Frequency__pc=2;
                  }
                           if(eachCustomerTempRec.OftenTesting__c=='1-3 times per month')
                {
              
                ac.Test_Frequency__pc=2;
                  }
                  
                   if(eachCustomerTempRec.OftenTesting__c=='Less than once a month')
                {
              
                ac.Test_Frequency__pc=1;
                  }
                 

                 if(eachCustomerTempRec.Gender__c=='M')
                  {
                    ac.Gender__pc='Male';
                  }
                    if(eachCustomerTempRec.Gender__c=='F')
                  {
                    ac.Gender__pc='Female';
                  }
                  
                  
                   if(eachCustomerTempRec.EmailPrivacy__c=='Y')
                  {
                    ac.Email_Privacy__pc='LFS';
                    ac.Phone_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.PhonePrivacy__c=='Y')
                  {
                    ac.Phone_Privacy__pc='LFS';
                    ac.Email_Privacy__pc='LFS';
                  }
               
                  
                   updateAccounts.add(ac);
                  
                  deleteProcessedTempCustomers.add(eachCustomerTempRec);
                  
                 system.debug('Vicky==>'+updateAccounts);
                    List<Device__C> oDevices = OldDevicesRegisteredMap.get(ListAccount[0].id);
                   if(oDevices!=null)
                    {
                   for(Device__c d:oDevices)
                   {
                       if(d.Serial_Number__c==eachCustomerTempRec.SerialNumber__c)
                       {
                           DeviceExisted = true;
                       }
                   }
                    }
                   
                   if(DeviceExisted==false)
                   {
                      List<Product2> productList = productUtilities.getProductsForSerialNumber(eachCustomerTempRec.SerialNumber__c, 'US');
                   
                   //Device Creation
                   if(!productList.isEmpty())
                   {
                       Device__C newDevice = new Device__c();
                       newDevice.Serial_Number__c = eachCustomerTempRec.SerialNumber__c;
                       newDevice.Patient__c = ListAccount[0].id;
                       newDevice.Product__c = ProductList[0].Id;
                       insertDevices.add(newDevice);
                   }
                    
                   }
                   
                        }
                   catch (Exception e)
                 {
                     system.debug('erro Message'+e.getMessage());
                 }
                 
               }                
               else
                  {
                  
                      System.debug('entered else loop');
                                  
                   //if(listAccount.isEmpty) 
              // {
                 try{Account acc = new Account();
                 //updateAccount.Confirm_Deceased__pc = true;
                // acc.Id = ListAccount[0].id;
                 acc.Direct_Mail_Privacy__pc = eachCustomerTempRec.DirectMailPrivacy__c;
                 acc.Email_Privacy__pc = eachCustomerTempRec.EmailPrivacy__c;
                 acc.Phone_Privacy__pc = eachCustomerTempRec.PhonePrivacy__c;
                 acc.SMS_Privacy__pc = eachCustomerTempRec.SMSPrivacy__c;
                 acc.PersonEmail=eachCustomerTempRec.Email__c;
                 acc.LastName=eachCustomerTempRec.LastName__c;
                 acc.FirstName=eachCustomerTempRec.FirstName__c;
                 System.debug('success'+eachCustomerTempRec.PrimaryStreet__c);
                 acc.ShippingStreet=eachCustomerTempRec.PrimaryStreet__c;
                 acc.ShippingState=eachCustomerTempRec.PrimaryStateProvince__c;
                 acc.ShippingPostalCode=eachCustomerTempRec.PrimaryZipPostalCode__c;
                 acc.ShippingCountry='US';
                 acc.ShippingCity=eachCustomerTempRec.PrimaryCity__c;
                 acc.SerialNumber__c =eachCustomerTempRec.SerialNumber__c;
                 acc.TypeOfFile__c=eachCustomerTempRec.TypeOfFile__c;
                //acc.Test_Frequency__pc=eachCustomerTempRec.Often_Testing__c;
                  acc.PersonHomePhone = eachCustomerTempRec.HomePhone__c;
                  acc.PersonMobilePhone = eachCustomerTempRec.Mobile__c;
                  acc.PersonBirthdate = eachCustomerTempRec.Birthdate__c;
                  acc.PersonTitle=eachCustomerTempRec.Title__c;
                 acc.Suffix=eachCustomerTempRec.Suffix__c;
                 acc.RecordTypeId = rt.id;
                 
//**************** Manage Methods >> IT will convert value to treatment method as multipicklist *****************************************
          
           String convertManagemethod = eachCustomerTempRec.ManageMethods__c;
                  convertManagemethod = convertManagemethod.trim();
                convertManagemethod = convertManagemethod.replaceAll('(\\s+)','');
                  
                 //  system.debug('convertManagemethod ==> ' +convertManagemethod );
                  
                 String[] convertedMethods = convertManagemethod.split(';');
                 
   

                 system.debug('convertedMethods ==> ' +convertedMethods );
                  //final string NUMBER_OF_HOURS_IN_A_DAY = 'Insulin';
                  //constant ss = insulin;
                   //final Integer thirdNumber = 3;
                  // final Integer twonumber = 2;
                  // final Integer onenumber = 1;
                  Map<Integer,String> mapManageMethods = new Map<Integer,String>();
                  mapManageMethods.put(1,'Diet');
                  mapManageMethods.put(2,'Pills');
                  mapManageMethods.put(3,'Insulin Shots');
                  mapManageMethods.put(4,'Insulin Pump');
                  mapManageMethods.put(5,'Exercise');
                  mapManageMethods.put(6,'Other');
                  mapManageMethods.put(7,'Shots other than insulin');
                  
                  string treatMethod='';
                  for(String s:convertedMethods)
                  {
                    String s1 = mapManageMethods.get(Integer.valueof(s));
                    system.debug('s1  ==> ' +s1 );
                    
                    //acc.TreatmentMethod__c+=s1+'+;';
                    treatMethod+= s1+';';
                    //treatMethod = s1;
                  }
                 acc.TreatmentMethod__c = treatMethod;
                 
//********************************** Managemethods ends here ***************************************************************************



          if(eachCustomerTempRec.OftenTesting__c=='4+ times per day')
                {
              
                acc.Test_Frequency__pc=28;
                  }                
                  if(eachCustomerTempRec.OftenTesting__c=='3 times per day')
                {
              
                acc.Test_Frequency__pc=21;
                  }
              
                    if(eachCustomerTempRec.OftenTesting__c=='2 times per day')
                {
              
                acc.Test_Frequency__pc=14;
                  }
                 
                     if(eachCustomerTempRec.OftenTesting__c=='1 time per day')
                {
              
                acc.Test_Frequency__pc=7;
                  }
                      if(eachCustomerTempRec.OftenTesting__c=='4-6 times per week')
                {
              
                acc.Test_Frequency__pc=5;
                  }
                        if(eachCustomerTempRec.OftenTesting__c=='1-3 times per week')
                {
              
                acc.Test_Frequency__pc=2;
                  }
                           if(eachCustomerTempRec.OftenTesting__c=='1-3 times per month')
                {
              
                acc.Test_Frequency__pc=2;
                  }
                  
                   if(eachCustomerTempRec.OftenTesting__c=='Less than once a month')
                {
              
                acc.Test_Frequency__pc=1;
                  }
                 
         
                 if(eachCustomerTempRec.Gender__c=='M')
                  {
                    acc.Gender__pc='Male';
                  }
                    if(eachCustomerTempRec.Gender__c=='F')
                  {
                    acc.Gender__pc='Female';
                  }
                  
                  
                  if(eachCustomerTempRec.EmailPrivacy__c=='Y')
                  {
                    acc.Email_Privacy__pc='LFS';
                    acc.Phone_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.PhonePrivacy__c=='Y')
                  {
                    acc.Phone_Privacy__pc='LFS';
                    acc.Email_Privacy__pc='LFS';
                  }
                
                  
                  insertAccounts.add(acc);
                   }
                 catch (Exception e)
                 {
                     system.debug('erro Message'+e.getMessage());
                 }

                   
                  deleteProcessedTempCustomers.add(eachCustomerTempRec);
               //}
              
                }
            
                  }
       Map<String,CustomerTempTable__c>  mapSerialNum = new Map<String,CustomerTempTable__c>();
            /*for(CustomerTempTable__c del : deleteProcessedTempCustomers)
            {
                mapSerialNum.put(del.SerialNumber__c,del);
            }*/
       if(!insertAccounts.isEmpty())
       {
           insert insertAccounts;
       }
        for(Account acc:insertAccounts)
       {
           system.debug('id is'+acc.serialNumber__c);
           List<Product2> ProductList1 = productUtilities.getProductsForSerialNumber(acc.SerialNumber__c, 'US');
           System.debug('ProductList1  is '+ProductList1);
                   //Device Creation
                   if(!ProductList1.isEmpty())
                   {
                        Device__C newDevice = new Device__c();
                        newDevice.Serial_Number__c = acc.SerialNumber__c;
                        newDevice.Patient__c = acc.id;
                        newDevice.Product__c = ProductList1[0].Id;
                        insertDevices.add(newDevice);
                   }
                   
       }
                
             
            
             if(!insertDevices.isEmpty())
            {
            insert insertDevices;
            }
            
                   if(!updateAccounts.isEmpty())
            {
              update updateAccounts;
            }
            
           
            
         }
   
   global void finish(Database.BatchableContext BC)
   {
           if(!deleteProcessedTempCustomers.isempty())
            {
              delete deleteProcessedTempCustomers;
            }
   }
  
}