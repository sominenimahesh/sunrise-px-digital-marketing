@isTest
public class customtempduplicationTest
{
    static testMethod void customtempduptest()
    {        
           Test.startTest();
          // List<CustomerTempTable__c>customerList =[select id ,FirstName__c,LastName__c,Email__c,Mobile__c,Country__c,PrimaryCity__c,PrimaryStateProvince__c,PrimaryStreet__c,PrimaryZipPostalCode__c from CustomerTempTable__c Where TypeOfFile__c='BRC'];
        RecordType rt = [select id,Name from RecordType where SobjectType='Account' and Name='Patient - US' Limit 1];   
        
        Account acc = New Account();
        acc.FirstName='Test2';
        acc.Lastname='Test12';
        acc.Personemail='xyz@gmail.com';
        acc.PersonMobilePhone = '+98406+456664';
        acc.ShippingStreet='vgs';
        acc.ShippingState='KAR';
        acc.ShippingCity='Bangalore';
        acc.shippingPostalCode ='600011';
        acc.ShippingCountry='US';
        acc.PersonHomePhone='+96969+333333';
        acc.Phone_Privacy__pc='LFS';
        acc.SMS_Privacy__pc='LFS';
        acc.Email_Privacy__pc='LFS';
        acc.Direct_Mail_Privacy__pc='Opt out';
        acc.TypeOfFile__c='BRC';
        acc.Age__c=28;
        acc.SerialNumber__c='V1234567';
        acc.TreatmentMethod__c = 'insulin shots;other;pills';
        acc.RecordTypeId = rt.id;
        Insert acc; 
        Product2 testProduct = new Product2();
   testProduct.Name = 'lancet1';
   testProduct.CurrencyIsoCode='USD';
   testProduct.isActive=true;  
        testProduct.Track_as_Asset__c = true;
   testProduct.Serial_Number_Mask__c = 'V???????';
   testProduct.Market__c = 'US';
   
   insert testProduct; 
   
    Id pricebookId = Test.getStandardPricebookId();
    
    PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = testProduct.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Gift Book - US', isActive=true);
        insert customPB;
        
         PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = testProduct.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
   List<Product2> productList = productUtilities.getProductsForSerialNumber(acc.SerialNumber__c, 'US');
   Device__c testDevice = new Device__c();
   testDevice.Serial_Number__c = 'V1234567';
   testDevice.Product__c = productList[0].Id;
   testDevice.Patient__c = acc.Id;
   
   insert testDevice;
   System.debug('test Device is'+testDevice);
        
         CustomerTempTable__c  cu = new CustomerTempTable__c();
         cu.FirstName__c ='Test2';
         cu.LastName__c ='Test12'; 
        // cu.Birthdate__c =Date.newinstance(1975, 9,28);
         cu.Email__c='xyz@gmail.com'; 
         cu.Mobile__c =  '+98406+456664'; 
         cu.Country__c = 'US'; 
         cu.PrimaryCity__c = 'Bangalore'; 
         cu.PrimaryStateProvince__c ='KAR'; 
         cu.PrimaryStreet__c='VGS'; 
         cu.PrimaryZipPostalCode__c ='600011';
         cu.PrimaryStreet__c='vgs';
         cu.TypeOfFile__c='BRC';
         cu.Age__c=28;
         cu.SerialNumber__c='V1234567';                 
         Cu.DirectMailPrivacy__c='Y';
         cu.PhonePrivacy__c='Y';          
         cu.EmailPrivacy__c='Y';
         cu.SMSPrivacy__c='Y';
         cu.Gender__c = 'F';
         cu.ManageMethods__c = '3;2;1';
          insert cu; 
        
        
         CustomerTempTable__c  cu1 = new CustomerTempTable__c();
         cu1.FirstName__c ='Test2';
         cu1.LastName__c ='Test12'; 
         //cu1.Birthdate__c =Date.newinstance(1975, 9,28);
         cu1.Email__c='xy@gmail.com'; 
         cu1.Mobile__c =  '+98406+456664'; 
         cu1.Country__c = 'US'; 
         cu1.PrimaryCity__c = 'Bangalore'; 
         cu1.PrimaryStateProvince__c ='KAR'; 
         cu1.PrimaryStreet__c='VGS'; 
         cu1.PrimaryZipPostalCode__c ='600011';
         cu1.PrimaryStreet__c='vgs';
         cu1.TypeOfFile__c='BRC';
         cu1.Age__c=55;
         Cu1.DirectMailPrivacy__c='Y';
         cu1.PhonePrivacy__c='Y';
         cu1.EmailPrivacy__c='Y';
         cu1.SMSPrivacy__c='Y';
         cu1.SerialNumber__c='V1234567';
         cu1.Gender__c = 'M';
         cu1.ManageMethods__c = '3;2;1;';
          insert cu1; 
        
   
  /* Device__c testDevice1 = new Device__c();
   testDevice1.Serial_Number__c = 'V1234567';
   testDevice1.Product__c = testProduct.Id;
   testDevice1.Patient__c = acc.Id;
   
   insert testDevice1; */
        
        
   
        /*   Device__c dev = new Device__c();
           dev.Serial_Number__c = 'D123456';   
           dev.Product__c = p.Id;
           dev.Patient__c = acc.Id;
   
            insert dev;
   
           Device__c dev1 = new Device__c();
           dev1.Serial_Number__c = 'D123456';  
           dev1.Product__c = p.Id;
           dev1.Patient__c =acc.Id;
   
             insert dev1;*/
             
           Test.stopTest();
        customtempduplication objBatch = new customtempduplication();
         ID batchprocessid = Database.executeBatch(objBatch);
    } 
    
   
    
        
    
    
}