<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <fields>
        <fullName>AcquisitionChannel__c</fullName>
        <description>Acquisition Channel to which this Business rule applicable for.</description>
        <externalId>false</externalId>
        <inlineHelpText>Offer will be applicable only for selected acquisition channels, Default will be considered as offer is applicable for all the acquisition channels.</inlineHelpText>
        <label>Acquisition Channel</label>
        <picklist>
            <picklistValues>
                <fullName>Email registration</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Letter form</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Phone registration</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Registration card</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Web registration</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Unknown</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>AgeGreaterThan__c</fullName>
        <description>Age Greater Than to which this Business rule applicable for.</description>
        <externalId>false</externalId>
        <inlineHelpText>Offer will be applicable only for selected customer where age is greater then selected date, Default will be considered as offer is applicable for all the customers.</inlineHelpText>
        <label>Age Greater Than</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AgeLessthan__c</fullName>
        <description>Age Less than to which this Business rule applicable for.</description>
        <externalId>false</externalId>
        <inlineHelpText>Offer will be applicable only for selected customer where age is less then selected date, Default will be considered as offer is applicable for all the customers.</inlineHelpText>
        <label>Age Less than</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AllowedProductCount__c</fullName>
        <externalId>false</externalId>
        <label>Allowable Offer Count</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CustomerType__c</fullName>
        <description>Customer Type to which this Business rule applicable for.</description>
        <externalId>false</externalId>
        <inlineHelpText>Offer will be applicable only for selected customer type, Default will be considered as offer is applicable for all the customer type.</inlineHelpText>
        <label>Customer Type</label>
        <picklist>
            <picklistValues>
                <fullName>Patient</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Non Patient</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Days__c</fullName>
        <description>Days duration to check the allowable offer count</description>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(Duration__c, &apos;3 Months&apos;),&apos;90&apos;,(IF(ISPICKVAL(Duration__c, &apos;6 Months&apos;),&apos;180&apos;,(IF(ISPICKVAL(Duration__c, &apos;9 Months&apos;),&apos;270&apos;,(IF(ISPICKVAL(Duration__c, &apos;1 Year&apos;),&apos;360&apos;,(IF(ISPICKVAL(Duration__c, &apos;2 Years&apos;),&apos;730&apos;,(IF(ISPICKVAL(Duration__c, &apos;3 Years&apos;),&apos;1095&apos;,(IF(ISPICKVAL(Duration__c, &apos;4 Years&apos;),&apos;1460&apos;,(IF(ISPICKVAL(Duration__c, &apos;5 Years&apos;),&apos;1825&apos;,(IF(ISPICKVAL(Duration__c, &apos;6 Years&apos;),&apos;2190&apos;,(IF(ISPICKVAL(Duration__c, &apos;7 Years&apos;),&apos;2555&apos;,(IF(ISPICKVAL(Duration__c, &apos;8 Years&apos;),&apos;2920&apos;,(IF(ISPICKVAL(Duration__c, &apos;9 Years&apos;),&apos;3285&apos;,(IF(ISPICKVAL(Duration__c, &apos;10 Years&apos;),&apos;3650&apos;,&apos;&apos;)))))))))))))))))))))))))</formula>
        <inlineHelpText>Days duration to check the allowable offer count</inlineHelpText>
        <label>Days</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Duration__c</fullName>
        <description>Duration to check the allowable offer count</description>
        <externalId>false</externalId>
        <inlineHelpText>Duration to check the allowable offer count</inlineHelpText>
        <label>Duration</label>
        <picklist>
            <picklistValues>
                <fullName>3 Months</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>6 Months</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>9 Months</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>1 Year</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2 Years</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>3 Years</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>4 Years</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5 Years</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>6 Years</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>7 Years</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>8 Years</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>9 Years</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>10 Years</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>FrequencyPerWeek__c</fullName>
        <description>Test Frequency per week</description>
        <externalId>false</externalId>
        <label>Test Frequency Per Week</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>IsActiveRule__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <label>Is Active Rule?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsHousehold__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Address check if one or more people who live in the same address</description>
        <externalId>false</externalId>
        <label>Household Enabled</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Legacy_Record_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Temp Field for Question Load</description>
        <externalId>true</externalId>
        <label>Legacy Record Id</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>NonEmptyFields__c</fullName>
        <description>Please provide some fields to which this Business rule applicable for.</description>
        <externalId>false</externalId>
        <inlineHelpText>Offer will be applicable only for customer where custom information is non empty for selected fields, Default will be considered as offer is applicable for all the customers fields, Please use the SFDC API name for non empty fields.</inlineHelpText>
        <label>Non Empty Fields</label>
        <picklist>
            <picklistValues>
                <fullName>Mobile</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PersonBirthdate</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PersonEmail</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>ProductType__c</fullName>
        <description>Type of the product (Self Reported &amp; Registered)</description>
        <externalId>false</externalId>
        <label>Type</label>
        <picklist>
            <picklistValues>
                <fullName>Self Reported</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Registered</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ProductUS__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup to US products only</description>
        <externalId>false</externalId>
        <inlineHelpText>Please select Product for offer page.</inlineHelpText>
        <label>Product</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Product2.Market__c</field>
                <operation>equals</operation>
                <value>US</value>
            </filterItems>
            <filterItems>
                <field>Product2.IsLifeScanUSOfferProduct__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Promotion Rules (Product)</relationshipLabel>
        <relationshipName>PromotionProductUS</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProductUniqueId__c</fullName>
        <description>Hidden field used for product unique id</description>
        <externalId>false</externalId>
        <formula>ProductUS__r.Legacy_Record_Id__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Unique Id</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProgramAwareness__c</fullName>
        <description>Please provide Program Awareness to which this Business rule applicable for.</description>
        <externalId>false</externalId>
        <inlineHelpText>Offer will be applicable only for selected programs, Default will be considered as offer is applicable for all the programs.</inlineHelpText>
        <label>Program Awareness</label>
        <picklist>
            <picklistValues>
                <fullName>TV</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Promotion_Rule_Group__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Specifically used for Question type rules - allowing them to be grouped together.</inlineHelpText>
        <label>Promotion Rule Group</label>
        <referenceTo>Promotion_Rule_Group__c</referenceTo>
        <relationshipLabel>Promotion Rules</relationshipLabel>
        <relationshipName>Promotion_Rules</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>RegisteredYear__c</fullName>
        <description>Self Reported to which this Business rule applicable for.</description>
        <externalId>false</externalId>
        <inlineHelpText>Offer will be applicable only for selected products which ere registered after selected date, Default will be considered as offer is applicable for all the customers.</inlineHelpText>
        <label>Meter Registered Years More Than/Equal</label>
        <picklist>
            <picklistValues>
                <fullName>1</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>3</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>4</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>6</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>7</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>8</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>9</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>10</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ShippingCountry__c</fullName>
        <description>Shipping Country to which this Business rule applicable for.</description>
        <externalId>false</externalId>
        <inlineHelpText>Offer will be applicable only for selected countries, Default will be considered as offer is applicable for all the countries.</inlineHelpText>
        <label>Shipping Country</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ShippingState__c</fullName>
        <description>Shipping State to which this Business rule applicable for.</description>
        <externalId>false</externalId>
        <inlineHelpText>Offer will be applicable only for selected status, Default will be considered as offer is applicable for all the states.</inlineHelpText>
        <label>Shipping State</label>
        <picklist>
            <picklistValues>
                <fullName>VIRGINIA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ALABAMA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ALASKA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ARIZONA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ARKANSAS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CALIFORNIA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>COLORADO</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CONNECTICUT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DELAWARE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FLORIDA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>GEORGIA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>HAWAII</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IDAHO</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ILLINOIS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>INDIANA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IOWA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>KANSAS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>KENTUCKY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>LOUISIANA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MAINE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MARYLAND</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MASSACHUSETTS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MICHIGAN</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MINNESOTA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MISSISSIPPI</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MISSOURI</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MONTANA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NEBRASKA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NEVADA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NEW HAMPSHIRE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NEW JERSEY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NEW MEXICO</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NEW YORK</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NORTH CAROLINA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NORTH DAKOTA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OHIO</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OKLAHOMA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OREGON</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PENNSYLVANIA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>RHODE ISLAND</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SOUTH CAROLINA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SOUTH DAKOTA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TENNESSEE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TEXAS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>UTAH</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>VERMONT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WASHINGTON</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WEST VIRGINIA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WISCONSIN</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WYOMING</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>TreatmentMethod__c</fullName>
        <description>Offer will be applicable only for selected treatment methods, Default will be considered as offer is applicable for all the methods.</description>
        <externalId>false</externalId>
        <inlineHelpText>Offer will be applicable only for selected treatment methods, Default will be considered as offer is applicable for all the methods.</inlineHelpText>
        <label>Treatment Method</label>
        <picklist>
            <picklistValues>
                <fullName>Diet/Exercise</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Insulin pump</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Insulin shots 1-2 times per day</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Insulin shots - 3 or more per day</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Pills</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
</CustomObject>
