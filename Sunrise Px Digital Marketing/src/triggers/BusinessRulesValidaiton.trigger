/** * 
* File Name: BusinessRulesValidation
* Description : Trigger is being developed to execute offer business rules based on customer eligibility.
* Copyright : Johnson & Johnson
* * @author : Manmohan Singh |mmo325@its.jnj.com | manmohan.singh1@cognizant.com

* Modification Log =============================================================== 
* 		Ver  |Date 			|Author 				|Modification
*		1.0  |23/07/2015	|@mmo325@its.jnj.com	|New Trigger created
* */ 
trigger BusinessRulesValidaiton on Account (before insert,after insert, after update) {
    
    if(trigger.isBefore && Trigger.isInsert){
        try{
            new UpdateDOBBasedOnAge(Trigger.new); 
        }catch(Exception ex){
            system.debug('UpdateDOBBasedOnAge : '+ex.getMessage());
        }
    }else{
        if(checkRecursive.runOnce())
        {
            try{ 
                new OfferBusinessRulesUSService(trigger.new);
            }Catch(Exception ex){
                system.debug('OfferBusinessRulesUSService : '+ex.getMessage());
            }
            try{ 
                new AccountPrivacyFlags().EmailNSmsOptInOptOutFlags(trigger.new);
            }Catch(Exception ex){
                system.debug('EmailNSmsOptInOptOutFlags : '+ex.getMessage());
            }
        }
    }
}