trigger PopulatedRegisteredDatetoAccount on Device__c (After Insert) 
{
    List<id> accId = new List<id>();
    
    if(Trigger.isAfter && Trigger.isInsert)
    {
         for(Device__c deviceValues : Trigger.new)   
         {
             accId.add(deviceValues.Patient__c);
             
             system.debug('ID==>'+accId );
         }
         
         list<Device__c> deviceData = [SELECT id , Patient__c, name ,Registered_Date__c FROM Device__c where Patient__c IN:accId];
         
         system.debug('deviceData==>'+deviceData);
         
         Date deviceRegisteredDate;
         
         for(Device__c deviceRegisteredDateValue : deviceData)
         {
             deviceRegisteredDate = deviceRegisteredDateValue.Registered_Date__c;
         }
        system.debug('deviceRegisteredDate==>'+deviceRegisteredDate);
        
        List<Account> updateAccIDValues = [SELECT id FROM  Account where ID IN:accId];
        
        system.debug('updateAccIDValues ==>'+updateAccIDValues );
        
        for(Account updateAccDeviceDate : updateAccIDValues )
        {
            updateAccDeviceDate.DeviceRegisteredDate__c = deviceRegisteredDate;
        } 
        
        update updateAccIDValues;
        
        system.debug('updateAccIDValues ==>'+updateAccIDValues );
    }
}