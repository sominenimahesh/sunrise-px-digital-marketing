<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>DirectMailOptOut</fullName>
        <field>Direct_Mail_Privacy__pc</field>
        <literalValue>Opt out</literalValue>
        <name>Direct Mail Opt Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EmailPrivacyOptOut</fullName>
        <description>Opt out from Emails</description>
        <field>Email_Privacy__pc</field>
        <literalValue>Opt out</literalValue>
        <name>Email Privacy Opt Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PersonAccountGenderUpdateAsFemale</fullName>
        <description>Update Gender as Female if Salutation is Miss/Mrs/Ms</description>
        <field>Gender__pc</field>
        <literalValue>Female</literalValue>
        <name>Person Account Gender Update as Female</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PersonAccountGenderUpdateAsMale</fullName>
        <field>Gender__pc</field>
        <literalValue>Male</literalValue>
        <name>Person Account Gender Update as Male</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PhonePrivacyOptOut</fullName>
        <field>Phone_Privacy__pc</field>
        <literalValue>Opt out</literalValue>
        <name>Phone Privacy Opt Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SMSPrivacyOptOut</fullName>
        <field>SMS_Privacy__pc</field>
        <literalValue>Opt out</literalValue>
        <name>SMS Privacy Opt Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OptOut</fullName>
        <actions>
            <name>DirectMailOptOut</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EmailPrivacyOptOut</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PhonePrivacyOptOut</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SMSPrivacyOptOut</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Confirm_Deceased__pc</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Person Account Gender from Salutation1</fullName>
        <actions>
            <name>PersonAccountGenderUpdateAsFemale</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Salutation</field>
            <operation>equals</operation>
            <value>Miss,Mrs,Ms</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Non Patient - US,Patient - US</value>
        </criteriaItems>
        <description>Populate Account Gender based on Salutation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Person Account Gender from Salutation2</fullName>
        <actions>
            <name>PersonAccountGenderUpdateAsMale</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Salutation</field>
            <operation>equals</operation>
            <value>Mr</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Non Patient - US,Patient - US</value>
        </criteriaItems>
        <description>Populate Account Gender based on Salutation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
